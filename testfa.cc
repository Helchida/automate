
#include "gtest/gtest.h"

#include "Automaton.h"

/**
 * Tests isValid
 */

TEST(AutomatonTestIsValid, Default) {
  fa::Automaton automate;
  EXPECT_FALSE(automate.isValid()); 
}

TEST(AutomatonTestIsValid, NoState) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_FALSE(automate.isValid()); 
}

TEST(AutomatonTestIsValid, NoSymbol) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_FALSE(automate.isValid()); 
}

TEST(AutomatonTestIsValid, Valid) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.isValid()); 
}

/**
 * Tests addSymbol
 */

TEST(AutomatonTestAddSymbol, Normal) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('B'));
}

TEST(AutomatonTestAddSymbol, SameSymbol) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_FALSE(automate.addSymbol('a'));
}

TEST(AutomatonTestAddSymbol, Epsilon) {
  fa::Automaton automate;
  EXPECT_FALSE(automate.addSymbol(fa::Epsilon));
}

TEST(AutomatonTestAddSymbol, EmptySymbol) {
  fa::Automaton automate;
  EXPECT_FALSE(automate.addSymbol(' '));
}

TEST(AutomatonTestAddSymbol, SpecialSymbol) {
  fa::Automaton automate;
  EXPECT_FALSE(automate.addSymbol('\n'));
  EXPECT_FALSE(automate.addSymbol('\0'));

}

/**
 * Tests removeSymbol
 */

TEST(AutomatonTestRemoveSymbol, Normal) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.removeSymbol('a'));
}

TEST(AutomatonTestRemoveSymbol, NotPresent) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_FALSE(automate.removeSymbol('b'));
}

TEST(AutomatonTestRemoveSymbol, Epsilon) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_FALSE(automate.removeSymbol(fa::Epsilon));
}

TEST(AutomatonTestRemoveSymbol, SpecialSymbol) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_FALSE(automate.removeSymbol('\n'));
  EXPECT_FALSE(automate.removeSymbol('\0'));
}

TEST(AutomatonTestRemoveSymbol, InOneTransition) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addTransition(0,'b',1));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.removeSymbol('b'));
  EXPECT_FALSE(automate.hasSymbol('b'));
  EXPECT_FALSE(automate.hasTransition(0,'b',1));
  EXPECT_TRUE(automate.hasTransition(0,'a',1));

}

TEST(AutomatonTestRemoveSymbol, InTwoTransition) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addTransition(0,'b',1));
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_TRUE(automate.addTransition(0,'a',2));
  EXPECT_TRUE(automate.removeSymbol('a'));
  EXPECT_FALSE(automate.hasTransition(0,'a',2));
  EXPECT_FALSE(automate.hasTransition(1,'a',2));
}

/**
 * Tests hasSymbol
 */

TEST(AutomatonTestHasSymbol, Normal) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.hasSymbol('a'));
}

TEST(AutomatonTestHasSymbol, NotPresent) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_FALSE(automate.hasSymbol('b'));
}

TEST(AutomatonTestHasSymbol, Epsilon) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_FALSE(automate.hasSymbol(fa::Epsilon));
}

TEST(AutomatonTestHasSymbol, SpecialSymbol) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_FALSE(automate.hasSymbol('\n'));
  EXPECT_FALSE(automate.hasSymbol('\0'));
}

/**
 * Tests countSymbols
 */

TEST(AutomatonTestCountSymbol, Normal) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_EQ(automate.countSymbols(),(std::size_t)2);
}

TEST(AutomatonTestCountSymbol, NoSymbol) {
  fa::Automaton automate;
  EXPECT_EQ(automate.countSymbols(),(std::size_t)0);
}

TEST(AutomatonTestCountSymbol, Epsilon) {
  fa::Automaton automate;
  EXPECT_FALSE(automate.addSymbol(fa::Epsilon));
  EXPECT_EQ(automate.countSymbols(),(std::size_t)0);
}

TEST(AutomatonTestCountSymbol, SpecialSymbol) {
  fa::Automaton automate;
  EXPECT_FALSE(automate.addSymbol('\n'));
  EXPECT_FALSE(automate.addSymbol('\0'));
  EXPECT_EQ(automate.countSymbols(),(std::size_t)0);
}

/**
 * Tests addState
 */

TEST(AutomatonTestAddState, OneState) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
}

TEST(AutomatonTestAddState, ManyState) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
}

TEST(AutomatonTestAddState, NegativeNumber) {
  fa::Automaton automate;
  EXPECT_FALSE(automate.addState(-1));
}

TEST(AutomatonTestAddState, SameNumber) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_FALSE(automate.addState(0));
}

/**
 * Tests removeState
 */

TEST(AutomatonTestRemoveState, OneState) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.hasState(0));
  EXPECT_TRUE(automate.removeState(0));
  EXPECT_FALSE(automate.hasState(0));
}

TEST(AutomatonTestRemoveState, ManyState) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.hasState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.hasState(1));
  EXPECT_TRUE(automate.removeState(1));
  EXPECT_TRUE(automate.removeState(0));
  EXPECT_FALSE(automate.hasState(1));
  EXPECT_FALSE(automate.hasState(0));
}

TEST(AutomatonTestRemoveState, StateNoPresent) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.hasState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.hasState(1));
  EXPECT_FALSE(automate.removeState(2));
}

TEST(AutomatonTestRemoveState, InOneTransition) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addTransition(0,'b',1));
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_TRUE(automate.removeState(0));
  EXPECT_FALSE(automate.hasTransition(0,'b',1));
  EXPECT_TRUE(automate.hasTransition(1,'a',2));
}

TEST(AutomatonTestRemoveState, InTwoTransition) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addTransition(0,'b',1));
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_TRUE(automate.removeState(1));
  EXPECT_FALSE(automate.hasTransition(0,'b',1));
  EXPECT_FALSE(automate.hasTransition(1,'a',2));
}

TEST(AutomatonTestRemoveState, StateInitialOrFinal) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  automate.setStateInitial(1);
  automate.setStateFinal(1);
  EXPECT_TRUE(automate.removeState(1));
  EXPECT_FALSE(automate.isStateInitial(1));
  EXPECT_FALSE(automate.isStateFinal(1));
}

TEST(AutomatonTestRemoveState, TwoStateInitialOrFinal) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  automate.setStateInitial(2);
  automate.setStateFinal(2);
  automate.setStateInitial(1);
  automate.setStateFinal(1);
  EXPECT_TRUE(automate.removeState(2));
  EXPECT_TRUE(automate.removeState(1));
  EXPECT_FALSE(automate.isStateInitial(1));
  EXPECT_FALSE(automate.isStateFinal(1));
  EXPECT_FALSE(automate.isStateInitial(2));
  EXPECT_FALSE(automate.isStateFinal(2));
}


/**
 * Tests hasState
 */

TEST(AutomatonTestHasState, OneState) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.hasState(0));
}

TEST(AutomatonTestHasState, ManyState) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.hasState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.hasState(1));
}

TEST(AutomatonTestHasState, NoState) {
  fa::Automaton automate;
  EXPECT_FALSE(automate.hasState(0));
  
}

/**
 * Tests countStates
 */

TEST(AutomatonTestCountStates, OneState) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_EQ(automate.countStates(),(std::size_t)1);
}

TEST(AutomatonTestCountStates, ManyState) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_EQ(automate.countStates(),(std::size_t)3);
}

TEST(AutomatonTestCountStates, ZeroState) {
  fa::Automaton automate;
  EXPECT_EQ(automate.countStates(),(std::size_t)0);
}

/**
 * Tests setStateInitial & isStateInitial
 */

TEST(AutomatonTestsetStateInitial, OneState) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.isStateInitial(0));
}

TEST(AutomatonTestsetStateInitial, ManyState) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  EXPECT_TRUE(automate.isStateInitial(0));
  EXPECT_TRUE(automate.isStateInitial(1));
}

TEST(AutomatonTestsetStateInitial, NoStateInitial) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_FALSE(automate.isStateInitial(0));
}

TEST(AutomatonTestsetStateInitial, NoStatePresent) {
  fa::Automaton automate;
  automate.setStateInitial(0);
  EXPECT_FALSE(automate.isStateInitial(0));
}

/**
 * Tests setStateFinal & isStateFinal
 */

TEST(AutomatonTestsetStateFinal, OneState) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  automate.setStateFinal(0);
  EXPECT_TRUE(automate.isStateFinal(0));
}

TEST(AutomatonTestsetStateFinal, ManyState) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  automate.setStateFinal(0);
  EXPECT_TRUE(automate.addState(1));
  automate.setStateFinal(1);
  EXPECT_TRUE(automate.isStateFinal(0));
  EXPECT_TRUE(automate.isStateFinal(1));
}

TEST(AutomatonTestsetStateFinal, NoStateFinal) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_FALSE(automate.isStateFinal(0));
}

TEST(AutomatonTestsetStateFinal, NoStatePresent) {
  fa::Automaton automate;
  automate.setStateFinal(0);
  EXPECT_FALSE(automate.isStateFinal(0));
}

/**
 * Tests addTransition
 */

TEST(AutomatonTestaddTransition, SimpleTransition) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.hasTransition(0,'a',1));
}

TEST(AutomatonTestaddTransition, ManyTransition) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.hasTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_TRUE(automate.hasTransition(1,'a',2));
  EXPECT_TRUE(automate.addTransition(1,'b',1));
  EXPECT_TRUE(automate.hasTransition(1,'b',1));
}

TEST(AutomatonTestaddTransition, SameTransition) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_FALSE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.hasTransition(0,'a',1));
}

TEST(AutomatonTestaddTransition, NotExistingTransition) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_FALSE(automate.hasTransition(0,'b',1));
  EXPECT_FALSE(automate.hasTransition(3,'a',1));
  EXPECT_FALSE(automate.hasTransition(0,'a',5));
}
/**
 * Tests removeTransition
 */

TEST(AutomatonTestremoveTransition, SimpleTransition) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.hasTransition(0,'a',1));
  EXPECT_TRUE(automate.removeTransition(0,'a',1));
  EXPECT_FALSE(automate.hasTransition(0,'a',1));
}

TEST(AutomatonTestremoveTransition, ManyTransition) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.hasTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(1,'b',1));
  EXPECT_TRUE(automate.hasTransition(1,'b',1));
  EXPECT_TRUE(automate.removeTransition(1,'b',1));
  EXPECT_TRUE(automate.removeTransition(0,'a',1));
  EXPECT_FALSE(automate.hasTransition(1,'b',1));
  EXPECT_FALSE(automate.hasTransition(0,'a',1));
}

TEST(AutomatonTestremoveTransition, AlreadyRemove) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.hasTransition(0,'a',1));
  EXPECT_TRUE(automate.removeTransition(0,'a',1));
  EXPECT_FALSE(automate.removeTransition(0,'a',1));
  EXPECT_FALSE(automate.hasTransition(0,'a',1));
}

TEST(AutomatonTestremoveTransition, NotExistingTransition) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.hasTransition(0,'a',1));
  EXPECT_FALSE(automate.removeTransition(0,'a',2));
}

/**
 * Tests hasTransition
 */

TEST(AutomatonTesthasTransition, TransitionValid) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.hasTransition(0,'a',1));
}

TEST(AutomatonTesthasTransition, TransitionNotValid) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_FALSE(automate.hasTransition(0,'b',1));
}

/**
 * Tests countTransitions
 */

TEST(AutomatonTestcountTransitions, OneTransition) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_EQ(automate.countTransitions(),(std::size_t)1);
}

TEST(AutomatonTestcountTransitions, ManyTransitions) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(1,'a',1));
  EXPECT_TRUE(automate.addTransition(0,'a',0));
  EXPECT_EQ(automate.countTransitions(),(std::size_t)3);
}

TEST(AutomatonTestcountTransitions, ZeroTransition) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_EQ(automate.countTransitions(),(std::size_t)0);
}

TEST(AutomatonTestcountTransitions, TransitionNoValid) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_FALSE(automate.addTransition(0,'a',2));
  EXPECT_EQ(automate.countTransitions(),(std::size_t)0);
}

/**
 * Tests PrettyPrint
 */

 TEST(AutomatonTestPrettyPrint, MyExample){
   fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(0,'b',1));
  EXPECT_TRUE(automate.addTransition(0,'a',0));
  EXPECT_TRUE(automate.addTransition(1,'b',2));
  EXPECT_TRUE(automate.addTransition(2,'a',0));
  std::ostream& os = std::cout;
  automate.prettyPrint(os);
 }

 TEST(AutomatonTestPrettyPrint, TpExample){
   fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  automate.setStateFinal(1);
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  EXPECT_TRUE(automate.addState(4));
  automate.setStateFinal(4);
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(0,'a',2));
  EXPECT_TRUE(automate.addTransition(0,'a',3));
  EXPECT_TRUE(automate.addTransition(1,'b',3));
  EXPECT_TRUE(automate.addTransition(3,'a',3));
  EXPECT_TRUE(automate.addTransition(2,'a',3));
  EXPECT_TRUE(automate.addTransition(2,'b',4));
  EXPECT_TRUE(automate.addTransition(3,'b',4));
  EXPECT_TRUE(automate.addTransition(4,'a',4));
  std::ostream& os = std::cout;
  automate.prettyPrint(os);
 }

/**
 * Tests hasEpsilonTransition
 */
TEST(AutomatonTestHasEpsilonTransition, HaveEpsilonTransition){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addTransition(0,fa::Epsilon,1));
  EXPECT_TRUE(automate.hasEpsilonTransition());
}

TEST(AutomatonTestHasEpsilonTransition, NotHaveEpsilonTransition){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_FALSE(automate.hasEpsilonTransition());
}

 /**
 * Tests isDeterministic
 */

TEST(AutomatonTestIsDeterministic, ValidEx6){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addTransition(0,'a',0));
  EXPECT_TRUE(automate.addTransition(0,'b',1));
  EXPECT_TRUE(automate.addTransition(1,'b',1));
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'b',2));
  EXPECT_TRUE(automate.isDeterministic());
}

TEST(AutomatonTestIsDeterministic, ValidEx8){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  automate.setStateFinal(1);
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addState(3));
  automate.setStateFinal(3);
  EXPECT_TRUE(automate.addState(4));
  EXPECT_TRUE(automate.addState(5));
  automate.setStateFinal(5);
  EXPECT_TRUE(automate.addState(6));
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(1,'b',3));
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'a',4));
  EXPECT_TRUE(automate.addTransition(2,'b',3));
  EXPECT_TRUE(automate.addTransition(3,'b',6));
  EXPECT_TRUE(automate.addTransition(3,'a',5));
  EXPECT_TRUE(automate.addTransition(4,'a',4));
  EXPECT_TRUE(automate.addTransition(4,'b',5));
  EXPECT_TRUE(automate.addTransition(5,'b',6));
  EXPECT_TRUE(automate.addTransition(6,'a',5));
  EXPECT_TRUE(automate.isDeterministic());
}

TEST(AutomatonTestIsDeterministic, NoValidTwoInitial){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addTransition(0,'a',0));
  EXPECT_TRUE(automate.addTransition(0,'b',1));
  EXPECT_TRUE(automate.addTransition(1,'b',1));
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'b',2));
  EXPECT_FALSE(automate.isDeterministic());
}

TEST(AutomatonTestIsDeterministic, NoValidEx6TwoTransitionWithSameLetter){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addTransition(0,'a',0));
  EXPECT_TRUE(automate.addTransition(0,'b',0));
  EXPECT_TRUE(automate.addTransition(0,'b',1));
  EXPECT_TRUE(automate.addTransition(1,'a',1));
  EXPECT_TRUE(automate.addTransition(1,'b',1));
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'b',2));
  EXPECT_FALSE(automate.isDeterministic());
}

TEST(AutomatonTestIsDeterministic, NoInitialState){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addTransition(0,'a',0));
  EXPECT_TRUE(automate.addTransition(0,'b',1));
  EXPECT_TRUE(automate.addTransition(1,'b',1));
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'b',2));
  EXPECT_FALSE(automate.isDeterministic());  
}

TEST(AutomatonTestIsDeterministic, Empty){
  fa::Automaton automate;
  EXPECT_FALSE(automate.isDeterministic());
}

TEST(AutomatonTestIsDeterministic, EpsilonTransition){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  EXPECT_TRUE(automate.addState(4));
  EXPECT_EQ(automate.countStates(), (std::size_t)4);
  automate.setStateInitial(4);
  automate.setStateFinal(2);
  automate.addSymbol('a');
  automate.addSymbol('b');
  EXPECT_EQ(automate.countSymbols(), (std::size_t)2);
  EXPECT_TRUE(automate.addTransition(1, fa::Epsilon, 4));
  EXPECT_TRUE(automate.addTransition(1, 'a', 3));
  EXPECT_TRUE(automate.addTransition(2, 'b', 4));
  EXPECT_TRUE(automate.isDeterministic());
}

TEST(AutomatonTestIsDeterministic, NoFinalStates){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addTransition(0,'a',0));
  EXPECT_TRUE(automate.addTransition(0,'b',1));
  EXPECT_TRUE(automate.addTransition(1,'b',1));
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'b',2));
  EXPECT_TRUE(automate.isDeterministic());
}

/**
 * Tests isComplete
 */

TEST(AutomatonTestIsComplete, Valid){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addTransition(0,'a',0));
  EXPECT_TRUE(automate.addTransition(0,'b',1));
  EXPECT_TRUE(automate.addTransition(1,'b',1));
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'b',2));
  EXPECT_TRUE(automate.isComplete());
}

TEST(AutomatonTestIsComplete, ValidWithTwoLetterOneTransition){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addTransition(0,'a',0));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(0,'b',1));
  EXPECT_TRUE(automate.addTransition(1,'b',1));
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'b',2));
  EXPECT_TRUE(automate.isComplete());
}

TEST(AutomatonTestIsComplete, MissingOneTransition){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addTransition(0,'a',0));
  EXPECT_TRUE(automate.addTransition(0,'b',1));
  EXPECT_TRUE(automate.addTransition(1,'b',1));
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'a',2));
  EXPECT_FALSE(automate.isComplete());
}

TEST(AutomatonTestIsComplete, OneStateValid){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  automate.setStateFinal(0);
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addSymbol('c'));
  EXPECT_TRUE(automate.addSymbol('d'));
  EXPECT_TRUE(automate.addTransition(0,'a',0));
  EXPECT_TRUE(automate.addTransition(0,'b',0));
  EXPECT_TRUE(automate.addTransition(0,'c',0));
  EXPECT_TRUE(automate.addTransition(0,'d',0));
  EXPECT_TRUE(automate.isComplete());
}

TEST(AutomatonTestIsComplete, OneStateNoValid){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  automate.setStateFinal(0);
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addSymbol('c'));
  EXPECT_TRUE(automate.addSymbol('d'));
  EXPECT_TRUE(automate.addTransition(0,'a',0));
  EXPECT_TRUE(automate.addTransition(0,'b',0));
  EXPECT_FALSE(automate.isComplete());
}

TEST(AutomatonTestIsComplete, NoSymbol){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  automate.setStateFinal(0);
  EXPECT_FALSE(automate.isComplete());
}

TEST(AutomatonTestIsComplete, NoTransition){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  automate.setStateFinal(0);
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addSymbol('c'));
  EXPECT_TRUE(automate.addSymbol('d'));
  EXPECT_FALSE(automate.isComplete());
}


TEST(AutomatonTestIsComplete, NoState){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_FALSE(automate.addTransition(1,'b',3));
  EXPECT_FALSE(automate.addTransition(1,'a',2));
  EXPECT_FALSE(automate.addTransition(2,'a',3));
  EXPECT_FALSE(automate.isComplete());
}

TEST(AutomatonTestIsComplete, AddedRemovedTransition){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  automate.setStateFinal(3);
  EXPECT_TRUE(automate.addState(4));
  EXPECT_TRUE(automate.addTransition(1,'b',3));
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'a',3));
  EXPECT_TRUE(automate.addTransition(2,'b',4));
  EXPECT_TRUE(automate.addTransition(3,'a',4));
  EXPECT_TRUE(automate.addTransition(3,'b',4));
  EXPECT_TRUE(automate.addTransition(4,'a',4));
  EXPECT_TRUE(automate.addTransition(4,'b',4));
  EXPECT_TRUE(automate.removeTransition(4,'b',4));
  EXPECT_FALSE(automate.isComplete());
}

/**
 * Tests createComplete
 */

TEST(AutomatonTestCreateComplete, CreationIsValid){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addSymbol('c'));
  EXPECT_TRUE(automate.addSymbol('d'));
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(4));
  EXPECT_TRUE(automate.addState(5));
  automate.setStateFinal(5);
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_TRUE(automate.addTransition(1,'b',4));
  EXPECT_TRUE(automate.addTransition(2,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'b',2));
  EXPECT_TRUE(automate.addTransition(2,'c',5));
  EXPECT_TRUE(automate.addTransition(2,'d',5));
  EXPECT_TRUE(automate.addTransition(4,'c',5));
  EXPECT_TRUE(automate.addTransition(4,'d',5));
  fa::Automaton auto_complete=fa::Automaton::createComplete(automate);
  EXPECT_FALSE(automate.isComplete());
  EXPECT_TRUE(auto_complete.isComplete());
}

TEST(AutomatonTestCreateComplete, CreationIsValid2){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_TRUE(automate.addTransition(0,'b',2));
  fa::Automaton auto_complete=fa::Automaton::createComplete(automate);
  EXPECT_FALSE(automate.isComplete());
  EXPECT_TRUE(auto_complete.isComplete());
}

TEST(AutomatonTestCreateComplete, AlreadyComplete){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addState(3));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_TRUE(automate.addTransition(0,'b',2));
  EXPECT_TRUE(automate.addTransition(1,'b',3));
  EXPECT_TRUE(automate.addTransition(2,'a',3));
  EXPECT_TRUE(automate.addTransition(2,'b',3));
  EXPECT_TRUE(automate.addTransition(3,'a',3));
  EXPECT_TRUE(automate.addTransition(3,'b',3));
  fa::Automaton auto_complete=fa::Automaton::createComplete(automate);
  EXPECT_TRUE(automate.isComplete());
  EXPECT_TRUE(auto_complete.isComplete());
}

TEST(AutomatonTestCreateComplete, ZeroTransition){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(0);
  fa::Automaton auto_complete=fa::Automaton::createComplete(automate);
  EXPECT_FALSE(automate.isComplete());
  EXPECT_TRUE(auto_complete.isComplete());
}

/**
 * Tests createComplement
 */

TEST(AutomatonTestCreateComplement, OneStateNotFinal){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addTransition(0,'a',0));
  automate.setStateInitial(0);
  fa::Automaton auto_complement=fa::Automaton::createComplement(automate);
  EXPECT_FALSE(automate.isComplete());
  EXPECT_TRUE(auto_complement.isComplete());


  EXPECT_EQ(auto_complement.countStates(),(std::size_t)2);
  EXPECT_EQ(auto_complement.countTransitions(),(std::size_t)4);
  const std::string mot = "abbbbabbbaba";
  EXPECT_TRUE(auto_complement.match(mot));
  EXPECT_FALSE(automate.match(mot));
}

TEST(AutomatonTestCreateComplement, OneStateFinal){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addTransition(0,'a',0));
  automate.setStateInitial(0);
  automate.setStateFinal(0);
  fa::Automaton auto_complement=fa::Automaton::createComplement(automate);
  EXPECT_FALSE(automate.isComplete());
  EXPECT_TRUE(auto_complement.isComplete());

  EXPECT_EQ(auto_complement.countStates(),(std::size_t)2);
  EXPECT_EQ(auto_complement.countTransitions(),(std::size_t)4);
  const std::string mot = "ababaab";
  EXPECT_TRUE(auto_complement.match(mot));
  EXPECT_FALSE(automate.match(mot));
  const std::string mot2 = "aaaaa";
  EXPECT_FALSE(auto_complement.match(mot2));
  EXPECT_TRUE(automate.match(mot2));
}

TEST(AutomatonTestCreateComplement, ZeroTransition){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(0);
  fa::Automaton auto_complement=fa::Automaton::createComplement(automate);
  EXPECT_FALSE(automate.isComplete());
  EXPECT_TRUE(auto_complement.isComplete());

  EXPECT_EQ(auto_complement.countStates(),(std::size_t)4);
  EXPECT_EQ(auto_complement.countTransitions(),(std::size_t)8);
  const std::string mot = "abbbbabbbaba";
  EXPECT_TRUE(auto_complement.match(mot));
  EXPECT_FALSE(automate.match(mot));
  const std::string mot2 = "";
  EXPECT_FALSE(auto_complement.match(mot2));
  EXPECT_TRUE(automate.match(mot2));
}

TEST(AutomatonTestCreateComplement, AlreadyComplete){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addState(3));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_TRUE(automate.addTransition(0,'b',2));
  EXPECT_TRUE(automate.addTransition(1,'b',3));
  EXPECT_TRUE(automate.addTransition(2,'a',3));
  EXPECT_TRUE(automate.addTransition(2,'b',3));
  EXPECT_TRUE(automate.addTransition(3,'a',3));
  EXPECT_TRUE(automate.addTransition(3,'b',3));
  fa::Automaton auto_complement=fa::Automaton::createComplement(automate);
  EXPECT_TRUE(automate.isComplete());
  EXPECT_TRUE(auto_complement.isComplete());

  EXPECT_EQ(auto_complement.countStates(),(std::size_t)4);
  EXPECT_EQ(auto_complement.countTransitions(),(std::size_t)8);
  const std::string mot = "aaa";
  EXPECT_TRUE(auto_complement.match(mot));
  EXPECT_FALSE(automate.match(mot));
  const std::string mot2 = "aa";
  EXPECT_FALSE(auto_complement.match(mot2));
  EXPECT_TRUE(automate.match(mot2));
}

TEST(AutomatonTestCreateComplement, NoDeterministic){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addState(3));
  automate.setStateFinal(3);
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(0,'a',2));
  EXPECT_TRUE(automate.addTransition(1,'b',3));
  EXPECT_TRUE(automate.addTransition(1,'a',2));

  EXPECT_FALSE(automate.isDeterministic());
  EXPECT_EQ(automate.countStates(),(std::size_t)4);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)4);
  fa::Automaton auto_complement = fa::Automaton::createComplement(automate);
  EXPECT_EQ(auto_complement.countStates(),(std::size_t)5);
  EXPECT_EQ(auto_complement.countTransitions(),(std::size_t)10);
  EXPECT_TRUE(auto_complement.isDeterministic());
  EXPECT_TRUE(auto_complement.isComplete());

  const std::string mot = "aabb";
  EXPECT_TRUE(auto_complement.match(mot));
  EXPECT_FALSE(automate.match(mot));
  const std::string mot2 = "ab";
  EXPECT_FALSE(auto_complement.match(mot2));
  EXPECT_TRUE(automate.match(mot2));

}

/**
 * Tests createMirror  
 */

TEST(AutomatonTestCreateMirror, CreationIsValid){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addSymbol('c'));
  EXPECT_TRUE(automate.addSymbol('d'));
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(4));
  EXPECT_TRUE(automate.addState(5));
  automate.setStateFinal(5);
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_TRUE(automate.addTransition(1,'b',4));
  EXPECT_TRUE(automate.addTransition(2,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'b',2));
  EXPECT_TRUE(automate.addTransition(2,'c',5));
  EXPECT_TRUE(automate.addTransition(2,'d',5));
  EXPECT_TRUE(automate.addTransition(4,'c',5));
  EXPECT_TRUE(automate.addTransition(4,'d',5));
  fa::Automaton auto_mirror=fa::Automaton::createMirror(automate);
  
  EXPECT_EQ(auto_mirror.countStates(),(std::size_t)4);
  EXPECT_EQ(auto_mirror.countTransitions(),(std::size_t)8);
  const std::string mot = "dba";
  EXPECT_TRUE(auto_mirror.match(mot));
  EXPECT_FALSE(automate.match(mot));
  const std::string mot2 = "abc";
  EXPECT_FALSE(auto_mirror.match(mot2));
  EXPECT_TRUE(automate.match(mot2));
}

TEST(AutomatonTestCreateMirror, OneStateInitial){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  EXPECT_TRUE(automate.addTransition(1,'a',1));
  EXPECT_TRUE(automate.addTransition(1,'b',1));
  fa::Automaton auto_mirror=fa::Automaton::createMirror(automate);
  
  EXPECT_EQ(auto_mirror.countStates(),(std::size_t)1);
  EXPECT_EQ(auto_mirror.countTransitions(),(std::size_t)2);
  const std::string mot = "a";
  EXPECT_FALSE(auto_mirror.match(mot));
  EXPECT_FALSE(automate.match(mot));
}

TEST(AutomatonTestCreateMirror, OneStateFinal){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(1));
  automate.setStateFinal(1);
  EXPECT_TRUE(automate.addTransition(1,'a',1));
  EXPECT_TRUE(automate.addTransition(1,'b',1));
  fa::Automaton auto_mirror=fa::Automaton::createMirror(automate);

  EXPECT_EQ(auto_mirror.countStates(),(std::size_t)1);
  EXPECT_EQ(auto_mirror.countTransitions(),(std::size_t)2);
  const std::string mot = "a";
  EXPECT_FALSE(auto_mirror.match(mot));
  EXPECT_FALSE(automate.match(mot));
}


/**
 * Tests isLanguageEmpty
 */


TEST(AutomatonTestIsLanguageEmpty, isNotEmpty){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  automate.setStateFinal(3);
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(0,'b',2));
  EXPECT_TRUE(automate.addTransition(2,'a',3));
  EXPECT_FALSE(automate.isLanguageEmpty());
}

TEST(AutomatonTestIsLanguageEmpty, isNotEmptyBig){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  EXPECT_TRUE(automate.addState(4));
  EXPECT_TRUE(automate.addState(5));
  EXPECT_TRUE(automate.addState(6));
  EXPECT_TRUE(automate.addState(7));
  automate.setStateFinal(7);
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(0,'b',2));
  EXPECT_TRUE(automate.addTransition(1,'b',4));
  EXPECT_TRUE(automate.addTransition(2,'a',1));
  EXPECT_TRUE(automate.addTransition(2,'b',3));
  EXPECT_TRUE(automate.addTransition(3,'b',4));
  EXPECT_TRUE(automate.addTransition(3,'a',5));
  EXPECT_TRUE(automate.addTransition(4,'b',7));
  EXPECT_TRUE(automate.addTransition(4,'a',5));
  EXPECT_TRUE(automate.addTransition(5,'a',6));
  EXPECT_TRUE(automate.addTransition(5,'b',7));
  EXPECT_TRUE(automate.addTransition(6,'a',3));
  EXPECT_TRUE(automate.addTransition(6,'b',7));
  EXPECT_FALSE(automate.isLanguageEmpty());
}

TEST(AutomatonTestIsLanguageEmpty, ValidNotEmpty){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  EXPECT_TRUE(automate.addState(5));
  EXPECT_TRUE(automate.addState(4));
  automate.setStateInitial(1);
  automate.setStateFinal(4);
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'a',3));
  EXPECT_TRUE(automate.addTransition(2,'b',2));
  EXPECT_TRUE(automate.addTransition(3,'b',1));
  EXPECT_TRUE(automate.addTransition(3,'b',5));
  EXPECT_TRUE(automate.addTransition(5,'b',4));
  EXPECT_TRUE(automate.addTransition(4,'a',2));
  EXPECT_FALSE(automate.isLanguageEmpty());
}

TEST(AutomatonTestIsLanguageEmpty, isEmpty){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(1);
  EXPECT_TRUE(automate.addTransition(1,'a',0));
  EXPECT_TRUE(automate.addTransition(0,'b',2));
  EXPECT_TRUE(automate.isLanguageEmpty());
}

TEST(AutomatonTestIsLanguageEmpty, noInitialState){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  automate.setStateFinal(3);
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(0,'b',2));
  EXPECT_TRUE(automate.addTransition(2,'a',3));
  EXPECT_TRUE(automate.isLanguageEmpty());
}

TEST(AutomatonTestIsLanguageEmpty, noFinalState){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(0,'b',2));
  EXPECT_TRUE(automate.addTransition(2,'a',3));
  EXPECT_TRUE(automate.isLanguageEmpty());
}

TEST(AutomatonTestIsLanguageEmpty, OneStateInitialAndFinal){  
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  automate.setStateFinal(0);
  EXPECT_FALSE(automate.isLanguageEmpty());
}

TEST(AutomatonTestIsLanguageEmpty, manyInitial){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  automate.setStateInitial(2);
  EXPECT_TRUE(automate.addState(3));
  automate.setStateInitial(3);
  EXPECT_TRUE(automate.addState(4));
  EXPECT_TRUE(automate.addState(5));
  EXPECT_TRUE(automate.addState(6));
  EXPECT_TRUE(automate.addState(7));
  automate.setStateFinal(7);
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(0,'b',2));
  EXPECT_TRUE(automate.addTransition(1,'b',4));
  EXPECT_TRUE(automate.addTransition(2,'a',1));
  EXPECT_TRUE(automate.addTransition(2,'b',3));
  EXPECT_TRUE(automate.addTransition(3,'b',4));
  EXPECT_TRUE(automate.addTransition(3,'a',5));
  EXPECT_TRUE(automate.addTransition(4,'b',7));
  EXPECT_TRUE(automate.addTransition(4,'a',5));
  EXPECT_TRUE(automate.addTransition(5,'a',6));
  EXPECT_TRUE(automate.addTransition(5,'b',7));
  EXPECT_TRUE(automate.addTransition(6,'a',3));
  EXPECT_TRUE(automate.addTransition(6,'b',7));
  EXPECT_FALSE(automate.isLanguageEmpty());
}

TEST(AutomatonTestIsLanguageEmpty, manyFinal){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  automate.setStateFinal(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  EXPECT_TRUE(automate.addState(4));
  EXPECT_TRUE(automate.addState(5));
  automate.setStateFinal(5);
  EXPECT_TRUE(automate.addState(6));
  EXPECT_TRUE(automate.addState(7));
  automate.setStateFinal(7);
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(0,'b',2));
  EXPECT_TRUE(automate.addTransition(1,'b',4));
  EXPECT_TRUE(automate.addTransition(2,'a',1));
  EXPECT_TRUE(automate.addTransition(2,'b',3));
  EXPECT_TRUE(automate.addTransition(3,'b',4));
  EXPECT_TRUE(automate.addTransition(3,'a',5));
  EXPECT_TRUE(automate.addTransition(4,'b',7));
  EXPECT_TRUE(automate.addTransition(4,'a',5));
  EXPECT_TRUE(automate.addTransition(5,'a',6));
  EXPECT_TRUE(automate.addTransition(5,'b',7));
  EXPECT_TRUE(automate.addTransition(6,'a',3));
  EXPECT_TRUE(automate.addTransition(6,'b',7));
  EXPECT_FALSE(automate.isLanguageEmpty());
}

TEST(AutomatonIsLanguageEmpty, Basic5){
  fa::Automaton automate;
  automate.addSymbol('a');
  automate.addState(0);
  automate.addState(1);
  automate.addState(2);
  automate.addState(3);
  automate.addState(4);
  automate.addState(5);
  automate.addState(6);
  automate.setStateInitial(0);
  automate.setStateFinal(5);

  automate.addTransition(0,'a',1);
  automate.addTransition(0,'a',2);
  automate.addTransition(1,'a',3);
  automate.addTransition(1,'a',4);
  automate.addTransition(2,'a',5);
  automate.addTransition(2,'a',6);


  EXPECT_FALSE(automate.isLanguageEmpty());
}




/**
 * Tests removeNonAccessibleStates
 */

TEST(AutomatonTestRemoveNonAccessibleStates, WithNonAccessible){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addState(3));
  EXPECT_TRUE(automate.addState(4));
  EXPECT_TRUE(automate.addState(5));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(1,'b',2));
  EXPECT_TRUE(automate.addTransition(1,'b',3));
  EXPECT_TRUE(automate.addTransition(4,'a',2));
  EXPECT_TRUE(automate.addTransition(5,'b',4));
  EXPECT_EQ(automate.countStates(),(std::size_t)6);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)5);
  const std::string mot = "ab";
  EXPECT_TRUE(automate.match(mot));
  
  automate.removeNonAccessibleStates();
  EXPECT_EQ(automate.countStates(),(std::size_t)4);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)3);
  EXPECT_TRUE(automate.match(mot));
}

TEST(AutomatonTestRemoveNonAccessibleStates, WithoutNonAccessible){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addState(3));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(1,'b',2));
  EXPECT_TRUE(automate.addTransition(1,'b',3));
  EXPECT_EQ(automate.countStates(),(std::size_t)4);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)3);
  const std::string mot = "ab";
  EXPECT_TRUE(automate.match(mot));
  automate.removeNonAccessibleStates();
  EXPECT_EQ(automate.countStates(),(std::size_t)4);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)3);
  EXPECT_TRUE(automate.match(mot));
}

TEST(AutomatonTestRemoveNonAccessibleStates, OneStateNoInitial){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addTransition(0,'a',0));
  EXPECT_EQ(automate.countStates(),(std::size_t)1);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)1);
  const std::string mot = "a";
  EXPECT_FALSE(automate.match(mot));
  automate.removeNonAccessibleStates();
  EXPECT_EQ(automate.countStates(),(std::size_t)1);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)0);
  EXPECT_FALSE(automate.match(mot));
}

TEST(AutomatonTestRemoveNonAccessibleStates, OneStateInitial){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addTransition(0,'a',0));
  EXPECT_EQ(automate.countStates(),(std::size_t)1);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)1);
  const std::string mot = "a";
  EXPECT_FALSE(automate.match(mot));
  automate.removeNonAccessibleStates();
  EXPECT_EQ(automate.countStates(),(std::size_t)1);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)1);
  EXPECT_FALSE(automate.match(mot));
}

TEST(AutomatonTestRemoveNonAccessibleStates, OneNonAccessibleStateAlone){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addTransition(0,'a',0));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_EQ(automate.countStates(),(std::size_t)3);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)2);
  const std::string mot = "a";
  EXPECT_FALSE(automate.match(mot));
  automate.removeNonAccessibleStates();
  EXPECT_EQ(automate.countStates(),(std::size_t)2);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)2);
  EXPECT_FALSE(automate.match(mot));
}

TEST(AutomatonTestRemoveNonAccessibleStates, ManyInitial){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addState(3));
  automate.setStateInitial(3);
  EXPECT_TRUE(automate.addState(4));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(2,'b',1));
  EXPECT_TRUE(automate.addTransition(3,'a',2));
  EXPECT_TRUE(automate.addTransition(4,'b',3));
  EXPECT_EQ(automate.countStates(),(std::size_t)5);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)4);
  const std::string mot = "a";
  EXPECT_TRUE(automate.match(mot));
  automate.removeNonAccessibleStates();
  EXPECT_EQ(automate.countStates(),(std::size_t)4);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)3);
  EXPECT_TRUE(automate.match(mot));
}

/**
 * Tests removeNonCoAccessibleStates
 */

TEST(AutomatonTestRemoveNonCoAccessibleStates, RemoveOneState){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  automate.setStateFinal(1);
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(1,'b',3));
  EXPECT_TRUE(automate.addTransition(3,'a',1));
  EXPECT_TRUE(automate.addTransition(0,'b',2));
  EXPECT_TRUE(automate.addTransition(3,'a',0));
  const std::string mot = "a";
  EXPECT_TRUE(automate.match(mot));
  EXPECT_EQ(automate.countStates(),(std::size_t)4);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)5);
  automate.removeNonCoAccessibleStates();
  EXPECT_EQ(automate.countStates(),(std::size_t)3);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)4);
  EXPECT_TRUE(automate.isValid());
  EXPECT_TRUE(automate.match(mot));
}

TEST(AutomatonTestRemoveNonCoAccessibleStates, RemoveManyState){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  automate.setStateFinal(1);
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  automate.setStateInitial(3);
  EXPECT_TRUE(automate.addState(4));
  EXPECT_TRUE(automate.addTransition(0,'a',2));
  EXPECT_TRUE(automate.addTransition(1,'b',0));
  EXPECT_TRUE(automate.addTransition(3,'a',1));
  EXPECT_TRUE(automate.addTransition(4,'b',1));
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_EQ(automate.countStates(),(std::size_t)5);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)5);
  const std::string mot = "a";
  EXPECT_TRUE(automate.match(mot));
  automate.removeNonCoAccessibleStates();
  EXPECT_EQ(automate.countStates(),(std::size_t)3);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)2);
  EXPECT_TRUE(automate.isValid());
  EXPECT_TRUE(automate.match(mot));
}

TEST(AutomatonTestRemoveNonCoAccessibleStates, RemoveManyStateManyTrans){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  automate.setStateFinal(1);
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  automate.setStateInitial(3);
  EXPECT_TRUE(automate.addState(4));
  EXPECT_TRUE(automate.addState(5));
  EXPECT_TRUE(automate.addState(6));
  EXPECT_TRUE(automate.addTransition(0,'a',2));
  EXPECT_TRUE(automate.addTransition(1,'b',0));
  EXPECT_TRUE(automate.addTransition(3,'a',1));
  EXPECT_TRUE(automate.addTransition(4,'b',1));
  EXPECT_TRUE(automate.addTransition(4,'a',2));
  EXPECT_TRUE(automate.addTransition(1,'b',5));
  EXPECT_TRUE(automate.addTransition(5,'a',6));
  EXPECT_TRUE(automate.addTransition(6,'b',5));
  EXPECT_EQ(automate.countStates(),(std::size_t)7);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)8);
  const std::string mot = "a";
  EXPECT_TRUE(automate.match(mot));
  automate.removeNonCoAccessibleStates();
  EXPECT_EQ(automate.countStates(),(std::size_t)3);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)2);
  EXPECT_TRUE(automate.isValid());
  EXPECT_TRUE(automate.match(mot));
}

TEST(AutomatonTestRemoveNonCoAccessibleStates, OneStateNotFinal){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_EQ(automate.countStates(),(std::size_t)1);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)0);
  const std::string mot = "a";
  EXPECT_FALSE(automate.match(mot));
  automate.removeNonCoAccessibleStates();
  EXPECT_EQ(automate.countStates(),(std::size_t)1);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)0);
  EXPECT_TRUE(automate.isValid());
  EXPECT_FALSE(automate.match(mot));
}

TEST(AutomatonTestRemoveNonCoAccessibleStates, OneStateInitialAndFinal){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  automate.setStateFinal(0);
  EXPECT_TRUE(automate.addTransition(0,'b',0));
  EXPECT_EQ(automate.countStates(),(std::size_t)1);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)1);
  const std::string mot = "b";
  EXPECT_TRUE(automate.match(mot));
  automate.removeNonCoAccessibleStates();
  EXPECT_EQ(automate.countStates(),(std::size_t)1);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)1);
  EXPECT_TRUE(automate.isValid());
  EXPECT_TRUE(automate.match(mot));
}

TEST(AutomatonTestRemoveNonCoAccessibleStates, OneStateNotInitialAndNotFinal){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addTransition(0,'b',0));
  EXPECT_EQ(automate.countStates(),(std::size_t)1);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)1);
  const std::string mot = "a";
  EXPECT_FALSE(automate.match(mot));
  automate.removeNonCoAccessibleStates();
  EXPECT_EQ(automate.countStates(),(std::size_t)1);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)0);
  EXPECT_TRUE(automate.isValid());
  EXPECT_FALSE(automate.match(mot));

}

/**
 * Tests createProduct
 */


TEST(AutomatonTestCreateProduct, ExempleVideo){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addState(3));
  automate.setStateInitial(3);
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'a',3));
  EXPECT_TRUE(automate.addTransition(3,'b',2));
  EXPECT_TRUE(automate.addTransition(3,'b',3));

  fa::Automaton automate_bis;
  EXPECT_TRUE(automate_bis.addSymbol('a'));
  EXPECT_TRUE(automate_bis.addSymbol('b'));
  EXPECT_TRUE(automate_bis.addState(4));
  automate_bis.setStateInitial(4);
  EXPECT_TRUE(automate_bis.addState(5));
  automate_bis.setStateFinal(5);
  EXPECT_TRUE(automate_bis.addTransition(4,'a',4));
  EXPECT_TRUE(automate_bis.addTransition(4,'b',5));
  EXPECT_TRUE(automate_bis.addTransition(5,'a',4));

  fa::Automaton automate_product=automate_product.createProduct(automate,automate_bis);
  EXPECT_EQ(automate_product.countStates(),(std::size_t)6);
  EXPECT_EQ(automate_product.countTransitions(),(std::size_t)6);
  EXPECT_EQ(automate_product.countSymbols(),(std::size_t)2);

}

TEST(AutomatonTestCreateProduct, OneStateOneTransition){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  automate.setStateFinal(1);
  EXPECT_TRUE(automate.addTransition(1,'a',1));

  fa::Automaton automate_bis;
  EXPECT_TRUE(automate_bis.addSymbol('a'));
  EXPECT_TRUE(automate_bis.addSymbol('b'));
  EXPECT_TRUE(automate_bis.addState(4));
  automate_bis.setStateInitial(4);
  automate_bis.setStateFinal(4);
  EXPECT_TRUE(automate_bis.addTransition(4,'a',4));

  fa::Automaton automate_product=automate_product.createProduct(automate,automate_bis);
  EXPECT_EQ(automate_product.countStates(),(std::size_t)1);
  EXPECT_EQ(automate_product.countTransitions(),(std::size_t)1);
  EXPECT_EQ(automate_product.countSymbols(),(std::size_t)2);

}

TEST(AutomatonTestCreateProduct, OneStateTwoTransition){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  automate.setStateFinal(1);
  EXPECT_TRUE(automate.addTransition(1,'a',1));

  fa::Automaton automate_bis;
  EXPECT_TRUE(automate_bis.addSymbol('a'));
  EXPECT_TRUE(automate_bis.addSymbol('b'));
  EXPECT_TRUE(automate_bis.addState(4));
  automate_bis.setStateInitial(4);
  automate_bis.setStateFinal(4);
  EXPECT_TRUE(automate_bis.addTransition(4,'b',4));

  fa::Automaton automate_product=automate_product.createProduct(automate,automate_bis);
  EXPECT_EQ(automate_product.countStates(),(std::size_t)1);
  EXPECT_EQ(automate_product.countTransitions(),(std::size_t)0);
  EXPECT_EQ(automate_product.countSymbols(),(std::size_t)2);
}

TEST(AutomatonTestCreateProduct, OneSymboleNotInRhs){ 
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  automate.setStateFinal(1);
  EXPECT_TRUE(automate.addTransition(1,'a',1));

  fa::Automaton automate_bis;
  EXPECT_TRUE(automate_bis.addSymbol('a'));
  EXPECT_TRUE(automate_bis.addState(4));
  automate_bis.setStateInitial(4);
  automate_bis.setStateFinal(4);
  EXPECT_TRUE(automate_bis.addTransition(4,'a',4));

  fa::Automaton automate_product=automate_product.createProduct(automate,automate_bis);
  EXPECT_EQ(automate_product.countStates(),(std::size_t)1);
  EXPECT_EQ(automate_product.countTransitions(),(std::size_t)1);
  EXPECT_EQ(automate_product.countSymbols(),(std::size_t)1);

  EXPECT_FALSE(automate_product.match("abbbbabbbaba"));
  EXPECT_TRUE(automate_product.match("aaaaaaaaaaa"));
  EXPECT_TRUE(automate_product.match(""));
  std::ostream& os = std::cout;
  automate_product.prettyPrint(os);

}

TEST(AutomatonTestCreateProduct, OneSymboleNotInLhs){ 
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  automate.setStateFinal(1);
  EXPECT_TRUE(automate.addTransition(1,'a',1));

  fa::Automaton automate_bis;
  EXPECT_TRUE(automate_bis.addSymbol('a'));
  EXPECT_TRUE(automate_bis.addSymbol('b'));
  EXPECT_TRUE(automate_bis.addState(4));
  automate_bis.setStateInitial(4);
  automate_bis.setStateFinal(4);
  EXPECT_TRUE(automate_bis.addTransition(4,'b',4));

  fa::Automaton automate_product=automate_product.createProduct(automate,automate_bis);
  EXPECT_EQ(automate_product.countStates(),(std::size_t)1);
  EXPECT_EQ(automate_product.countTransitions(),(std::size_t)0);
  EXPECT_EQ(automate_product.countSymbols(),(std::size_t)1);
  EXPECT_FALSE(automate_product.match("abbbbabbbaba"));
  EXPECT_TRUE(automate_product.match(""));
  EXPECT_FALSE(automate_product.match("aaaaaaaaaa"));
  std::ostream& os = std::cout;
  automate_product.prettyPrint(os);

}

TEST(AutomatonTestCreateProduct, OneSymboleNotInLhs2){ 
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  automate.setStateFinal(1);
  EXPECT_TRUE(automate.addTransition(1,'a',1));

  fa::Automaton automate_bis;
  EXPECT_TRUE(automate_bis.addSymbol('a'));
  EXPECT_TRUE(automate_bis.addSymbol('b'));
  EXPECT_TRUE(automate_bis.addState(4));
  automate_bis.setStateInitial(4);
  automate_bis.setStateFinal(4);
  EXPECT_TRUE(automate_bis.addTransition(4,'a',4));

  fa::Automaton automate_product=automate_product.createProduct(automate,automate_bis);
  EXPECT_EQ(automate_product.countStates(),(std::size_t)1);
  EXPECT_EQ(automate_product.countTransitions(),(std::size_t)1);
  EXPECT_EQ(automate_product.countSymbols(),(std::size_t)1);
  EXPECT_FALSE(automate_product.match("abbbbabbbaba"));
  EXPECT_TRUE(automate_product.match(""));
  EXPECT_TRUE(automate_product.match("aaaaaaaaaa"));
  std::ostream& os = std::cout;
  automate_product.prettyPrint(os);

}

TEST(AutomatonTestCreateProduct, NotSameSymboles){ 
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  automate.setStateFinal(1);
  EXPECT_TRUE(automate.addTransition(1,'a',1));

  fa::Automaton automate_bis;
  EXPECT_TRUE(automate_bis.addSymbol('c'));
  EXPECT_TRUE(automate_bis.addSymbol('d'));
  EXPECT_TRUE(automate_bis.addState(4));
  automate_bis.setStateInitial(4);
  automate_bis.setStateFinal(4);
  EXPECT_TRUE(automate_bis.addTransition(4,'c',4));

  fa::Automaton automate_product=automate_product.createProduct(automate,automate_bis);
  EXPECT_EQ(automate_product.countStates(),(std::size_t)1);
  EXPECT_EQ(automate_product.countTransitions(),(std::size_t)0);
  EXPECT_EQ(automate_product.countSymbols(),(std::size_t)1);
  EXPECT_FALSE(automate_product.match("abbbbabbbaba"));
  EXPECT_TRUE(automate_product.match(""));
  std::ostream& os = std::cout;
  automate_product.prettyPrint(os);

}

TEST(AutomatonTestCreateProduct, SameNameState){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addState(3));
  automate.setStateInitial(3);
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'a',3));
  EXPECT_TRUE(automate.addTransition(3,'b',2));
  EXPECT_TRUE(automate.addTransition(3,'b',3));

  fa::Automaton automate_bis;
  EXPECT_TRUE(automate_bis.addSymbol('a'));
  EXPECT_TRUE(automate_bis.addSymbol('b'));
  EXPECT_TRUE(automate_bis.addState(1));
  automate_bis.setStateInitial(1);
  EXPECT_TRUE(automate_bis.addState(2));
  automate_bis.setStateFinal(2);
  EXPECT_TRUE(automate_bis.addTransition(1,'a',1));
  EXPECT_TRUE(automate_bis.addTransition(1,'b',2));
  EXPECT_TRUE(automate_bis.addTransition(2,'a',1));

  fa::Automaton automate_product=automate_product.createProduct(automate,automate_bis);
  EXPECT_EQ(automate_product.countStates(),(std::size_t)6);
  EXPECT_EQ(automate_product.countTransitions(),(std::size_t)6);
  EXPECT_EQ(automate_product.countSymbols(),(std::size_t)2);

}

TEST(AutomatonTestCreateProduct, NoTransition){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addState(3));
  automate.setStateInitial(3);

  fa::Automaton automate_bis;
  EXPECT_TRUE(automate_bis.addSymbol('a'));
  EXPECT_TRUE(automate_bis.addSymbol('b'));
  EXPECT_TRUE(automate_bis.addState(1));
  automate_bis.setStateInitial(1);
  EXPECT_TRUE(automate_bis.addState(2));
  automate_bis.setStateFinal(2);

  fa::Automaton automate_product=automate_product.createProduct(automate,automate_bis);
  EXPECT_EQ(automate_product.countStates(),(std::size_t)6);
  EXPECT_EQ(automate_product.countTransitions(),(std::size_t)0);
  EXPECT_EQ(automate_product.countSymbols(),(std::size_t)2);
  std::ostream& os = std::cout;
  automate_product.prettyPrint(os);

}

TEST(AutomatonTestCreateProduct, AlphabetDesordre){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addState(3));
  automate.setStateInitial(3);
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'a',3));
  EXPECT_TRUE(automate.addTransition(3,'b',2));
  EXPECT_TRUE(automate.addTransition(3,'b',3));

  fa::Automaton automate_bis;
  EXPECT_TRUE(automate_bis.addSymbol('b'));
  EXPECT_TRUE(automate_bis.addSymbol('a'));
  EXPECT_TRUE(automate_bis.addState(4));
  automate_bis.setStateInitial(4);
  EXPECT_TRUE(automate_bis.addState(5));
  automate_bis.setStateFinal(5);
  EXPECT_TRUE(automate_bis.addTransition(4,'a',4));
  EXPECT_TRUE(automate_bis.addTransition(4,'b',5));
  EXPECT_TRUE(automate_bis.addTransition(5,'a',4));

  fa::Automaton automate_product=automate_product.createProduct(automate,automate_bis);
  EXPECT_EQ(automate_product.countStates(),(std::size_t)6);
  EXPECT_EQ(automate_product.countTransitions(),(std::size_t)6);
  EXPECT_EQ(automate_product.countSymbols(),(std::size_t)2);

}

TEST(AutomatonTestCreateProduct, OneInitialInLhsOneFinalInRhs){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'a',3));
  EXPECT_TRUE(automate.addTransition(3,'b',2));
  EXPECT_TRUE(automate.addTransition(3,'b',3));

  fa::Automaton automate_bis;
  EXPECT_TRUE(automate_bis.addSymbol('b'));
  EXPECT_TRUE(automate_bis.addSymbol('a'));
  EXPECT_TRUE(automate_bis.addState(4));
  EXPECT_TRUE(automate_bis.addState(5));
  automate_bis.setStateFinal(5);
  EXPECT_TRUE(automate_bis.addTransition(4,'a',4));
  EXPECT_TRUE(automate_bis.addTransition(4,'b',5));
  EXPECT_TRUE(automate_bis.addTransition(5,'a',4));

  fa::Automaton automate_product=automate_product.createProduct(automate,automate_bis);
  EXPECT_EQ(automate_product.countStates(),(std::size_t)6);
  EXPECT_EQ(automate_product.countTransitions(),(std::size_t)6);
  EXPECT_EQ(automate_product.countSymbols(),(std::size_t)2);

}

/**
 * Tests hasEmptyIntersectionWith
 */

TEST(AutomatonTestHasEmptyIntersectionWith, ReturnFalse){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addState(3));
  automate.setStateInitial(3);
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'a',3));
  EXPECT_TRUE(automate.addTransition(3,'b',2));
  EXPECT_TRUE(automate.addTransition(3,'b',3));

  fa::Automaton automate_bis;
  EXPECT_TRUE(automate_bis.addSymbol('a'));
  EXPECT_TRUE(automate_bis.addSymbol('b'));
  EXPECT_TRUE(automate_bis.addState(4));
  automate_bis.setStateInitial(4);
  EXPECT_TRUE(automate_bis.addState(5));
  automate_bis.setStateFinal(5);
  EXPECT_TRUE(automate_bis.addTransition(4,'a',4));
  EXPECT_TRUE(automate_bis.addTransition(4,'b',5));
  EXPECT_TRUE(automate_bis.addTransition(5,'a',4));

  EXPECT_FALSE(automate.hasEmptyIntersectionWith(automate_bis));

}

TEST(AutomatonTestHasEmptyIntersectionWith, NoTransitionReturnTrue){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addState(3));
  automate.setStateInitial(3);

  fa::Automaton automate_bis;
  EXPECT_TRUE(automate_bis.addSymbol('a'));
  EXPECT_TRUE(automate_bis.addSymbol('b'));
  EXPECT_TRUE(automate_bis.addState(1));
  automate_bis.setStateInitial(1);
  EXPECT_TRUE(automate_bis.addState(2));
  automate_bis.setStateFinal(2);

  EXPECT_TRUE(automate.hasEmptyIntersectionWith(automate_bis));
  

}

TEST(AutomatonTestHasEmptyIntersectionWith, isTrue){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  automate.setStateFinal(1);
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(2,'b',1));
  EXPECT_TRUE(automate.addTransition(2,'b',0));
  std::ostream& os = std::cout;
  automate.prettyPrint(os);
  fa::Automaton automate_bis;
  EXPECT_TRUE(automate_bis.addSymbol('a'));
  EXPECT_TRUE(automate_bis.addSymbol('b'));
  EXPECT_TRUE(automate_bis.addState(4));
  automate_bis.setStateInitial(4);
  EXPECT_TRUE(automate_bis.addState(5));
  automate_bis.setStateFinal(5);
  EXPECT_TRUE(automate_bis.addState(1));
  automate_bis.setStateFinal(1);
  EXPECT_TRUE(automate_bis.addTransition(4,'a',1));
  EXPECT_TRUE(automate_bis.addTransition(1,'a',4));
  EXPECT_TRUE(automate_bis.addTransition(1,'b',5));
  EXPECT_TRUE(automate_bis.addTransition(5,'b',4));
  automate_bis.prettyPrint(os);
  EXPECT_TRUE(automate.hasEmptyIntersectionWith(automate_bis));
}

TEST(AutomatonTestHasEmptyIntersectionWith, isFalse){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  automate.setStateFinal(1);
  EXPECT_TRUE(automate.addState(2));
  automate.setStateInitial(2);
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(2,'b',1));
  EXPECT_TRUE(automate.addTransition(2,'b',0));
  std::ostream& os = std::cout;
  automate.prettyPrint(os);
  fa::Automaton automate_bis;
  EXPECT_TRUE(automate_bis.addSymbol('a'));
  EXPECT_TRUE(automate_bis.addSymbol('b'));
  EXPECT_TRUE(automate_bis.addState(4));
  automate_bis.setStateInitial(4);
  EXPECT_TRUE(automate_bis.addState(5));
  automate_bis.setStateFinal(5);
  EXPECT_TRUE(automate_bis.addState(1));
  automate_bis.setStateFinal(1);
  EXPECT_TRUE(automate_bis.addTransition(4,'a',1));
  EXPECT_TRUE(automate_bis.addTransition(1,'a',4));
  EXPECT_TRUE(automate_bis.addTransition(1,'b',5));
  EXPECT_TRUE(automate_bis.addTransition(5,'b',4));
  automate_bis.prettyPrint(os);
  
  EXPECT_FALSE(automate.hasEmptyIntersectionWith(automate_bis));
}

/**
 * Tests readString
 */

TEST(AutomatonTestReadString, ValidOnePath){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  automate.setStateFinal(3);
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(1,'a',3));
  EXPECT_TRUE(automate.addTransition(0,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'b',3));
  std::ostream& os = std::cout;
  automate.prettyPrint(os);
  
  const std::string mot = "ab";
  std::set<int> chemin;
  chemin=automate.readString(mot);

  
  for(auto it=chemin.begin(); it!=chemin.end();++it){
    os  << *it << "  " ;
  }
  os << "\n";

  EXPECT_EQ(chemin.size(),(std::size_t)1);
  EXPECT_EQ(chemin.count(3),(std::size_t)1);
}

TEST(AutomatonTestReadString, ValidTwoPath){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  automate.setStateFinal(3);
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(1,'a',3));
  EXPECT_TRUE(automate.addTransition(0,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'b',3));
  std::ostream& os = std::cout;
  automate.prettyPrint(os);
  
  const std::string mot = "a";
  std::set<int> chemin;
  chemin=automate.readString(mot);

  
  for(auto it=chemin.begin(); it!=chemin.end();++it){
    os  << *it << "  " ;
  }
  os << "\n";

  EXPECT_EQ(chemin.size(),(std::size_t)2);
  EXPECT_EQ(chemin.count(1),(std::size_t)1);
  EXPECT_EQ(chemin.count(2),(std::size_t)1);
}

TEST(AutomatonTestReadString, ValidThreePathTwoInit){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  automate.setStateFinal(3);
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(1,'a',3));
  EXPECT_TRUE(automate.addTransition(0,'a',2));
  EXPECT_TRUE(automate.addTransition(2,'b',3));
  std::ostream& os = std::cout;
  automate.prettyPrint(os);
  
  const std::string mot = "a";
  std::set<int> chemin;
  chemin=automate.readString(mot);

  
  for(auto it=chemin.begin(); it!=chemin.end();++it){
    os  << *it << "  " ;
  }
  os << "\n";

  EXPECT_EQ(chemin.size(),(std::size_t)3);
  EXPECT_EQ(chemin.count(1),(std::size_t)1);
  EXPECT_EQ(chemin.count(2),(std::size_t)1);
  EXPECT_EQ(chemin.count(3),(std::size_t)1);
}

TEST(AutomatonTestReadString, NoWord){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(1,'b',0));
  std::ostream& os = std::cout;
  automate.prettyPrint(os);
  
  const std::string mot = "";
  std::set<int> chemin;
  chemin=automate.readString(mot);

  
  for(auto it=chemin.begin(); it!=chemin.end();++it){
    os  << *it << "  " ;
  }
  os << "\n";

  EXPECT_EQ(chemin.size(),(std::size_t)2);
  EXPECT_EQ(chemin.count(0),(std::size_t)1);
  EXPECT_EQ(chemin.count(1),(std::size_t)1);
}

TEST(AutomatonTestReadString, ManyInitialAndManyResult){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  EXPECT_TRUE(automate.addState(4));
  automate.setStateInitial(4);
  EXPECT_TRUE(automate.addState(5));
  EXPECT_TRUE(automate.addState(6));
  automate.setStateFinal(6);
  EXPECT_TRUE(automate.addState(7));
  automate.setStateInitial(7);
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(0,'a',2));
  EXPECT_TRUE(automate.addTransition(1,'b',3));
  EXPECT_TRUE(automate.addTransition(2,'a',3));
  EXPECT_TRUE(automate.addTransition(3,'a',4));
  EXPECT_TRUE(automate.addTransition(3,'b',5));
  EXPECT_TRUE(automate.addTransition(3,'b',6));
  EXPECT_TRUE(automate.addTransition(4,'a',1));
  EXPECT_TRUE(automate.addTransition(5,'a',6));
  EXPECT_TRUE(automate.addTransition(5,'b',3));
  EXPECT_TRUE(automate.addTransition(6,'a',2));
  EXPECT_TRUE(automate.addTransition(6,'a',7));
  EXPECT_TRUE(automate.addTransition(7,'a',5));
  std::ostream& os = std::cout;
  
  const std::string mot = "abba";
  std::set<int> chemin;
  chemin=automate.readString(mot);

  
  for(auto it=chemin.begin(); it!=chemin.end();++it){
    os  << *it << "  " ;
  }
  os << "\n";

  EXPECT_EQ(chemin.size(),(std::size_t)3);
  EXPECT_EQ(chemin.count(2),(std::size_t)1);
  EXPECT_EQ(chemin.count(6),(std::size_t)1);
  EXPECT_EQ(chemin.count(7),(std::size_t)1);

  const std::string mot2 = "aa";
  std::set<int> chemin2;
  chemin2=automate.readString(mot2);

  
  for(auto it2=chemin2.begin(); it2!=chemin2.end();++it2){
    os  << *it2 << "  " ;
  }
  os << "\n";

  EXPECT_EQ(chemin2.size(),(std::size_t)2);
  EXPECT_EQ(chemin2.count(3),(std::size_t)1);
  EXPECT_EQ(chemin2.count(6),(std::size_t)1);
}

TEST(AutomatonTestReadString, ZeroRead){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(1,'b',0));
  std::ostream& os = std::cout;
  automate.prettyPrint(os);
  
  const std::string mot = "a";
  std::set<int> chemin;
  chemin=automate.readString(mot);

  
  for(auto it=chemin.begin(); it!=chemin.end();++it){
    os  << *it << "  " ;
  }
  os << "\n";

  EXPECT_EQ(chemin.size(),(std::size_t)0);
}

TEST(AutomatonTestReadString, OneStateNoInitial){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  std::ostream& os = std::cout;
  automate.prettyPrint(os);
  
  const std::string mot = "a";
  std::set<int> chemin;
  chemin=automate.readString(mot);

  
  for(auto it=chemin.begin(); it!=chemin.end();++it){
    os  << *it << "  " ;
  }
  os << "\n";

  EXPECT_EQ(chemin.size(),(std::size_t)0);
}

TEST(AutomatonTestReadString, OneStateInitialWithBoucle){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addTransition(0,'a',0));
  EXPECT_TRUE(automate.addTransition(0,'b',0));
  
  std::ostream& os = std::cout;
  automate.prettyPrint(os);
  
  const std::string mot = "ababbbaabbba";
  std::set<int> chemin;
  chemin=automate.readString(mot);

  
  for(auto it=chemin.begin(); it!=chemin.end();++it){
    os  << *it << "  " ;
  }
  os << "\n";

  EXPECT_EQ(chemin.size(),(std::size_t)1);
  EXPECT_EQ(chemin.count(0),(std::size_t)1);
}

TEST(AutomatonTestReadString, TwoStateWithBoucle){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addTransition(0,'a',0));
  EXPECT_TRUE(automate.addTransition(0,'b',1));
  EXPECT_TRUE(automate.addTransition(1,'a',1));
  
  std::ostream& os = std::cout;
  automate.prettyPrint(os);
  
  const std::string mot = "aaaaabaaaaaaa";
  std::set<int> chemin;
  chemin=automate.readString(mot);

  
  for(auto it=chemin.begin(); it!=chemin.end();++it){
    os  << *it << "  " ;
  }
  os << "\n";

  EXPECT_EQ(chemin.size(),(std::size_t)1);
  EXPECT_EQ(chemin.count(1),(std::size_t)1);

  const std::string mot2 = "aaaaabbaaaaaaa";
  std::set<int> chemin2;
  chemin2=automate.readString(mot2);

  
  for(auto it2=chemin2.begin(); it2!=chemin2.end();++it2){
    os  << *it2 << "  " ;
  }
  os << "\n";

  EXPECT_EQ(chemin2.size(),(std::size_t)0);
}

/**
 * Tests match
 */

TEST(AutomatonTestMatch, WordIsRecognized){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  automate.setStateFinal(3);
  EXPECT_TRUE(automate.addState(4));
  automate.setStateInitial(4);
  EXPECT_TRUE(automate.addState(5));
  EXPECT_TRUE(automate.addState(6));
  automate.setStateFinal(6);
  EXPECT_TRUE(automate.addState(7));
  automate.setStateInitial(7);
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(0,'a',2));
  EXPECT_TRUE(automate.addTransition(1,'b',3));
  EXPECT_TRUE(automate.addTransition(2,'a',3));
  EXPECT_TRUE(automate.addTransition(3,'a',4));
  EXPECT_TRUE(automate.addTransition(3,'b',5));
  EXPECT_TRUE(automate.addTransition(3,'b',6));
  EXPECT_TRUE(automate.addTransition(4,'a',1));
  EXPECT_TRUE(automate.addTransition(5,'a',6));
  EXPECT_TRUE(automate.addTransition(5,'b',3));
  EXPECT_TRUE(automate.addTransition(6,'a',2));
  EXPECT_TRUE(automate.addTransition(6,'a',7));
  EXPECT_TRUE(automate.addTransition(7,'a',5));
  
  const std::string mot = "abba";
  EXPECT_TRUE(automate.match(mot));

}

TEST(AutomatonTestMatch, WordIsNotRecognizedBegin){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  automate.setStateFinal(3);
  EXPECT_TRUE(automate.addState(4));
  automate.setStateInitial(4);
  EXPECT_TRUE(automate.addState(5));
  EXPECT_TRUE(automate.addState(6));
  automate.setStateFinal(6);
  EXPECT_TRUE(automate.addState(7));
  automate.setStateInitial(7);
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(0,'a',2));
  EXPECT_TRUE(automate.addTransition(1,'b',3));
  EXPECT_TRUE(automate.addTransition(2,'a',3));
  EXPECT_TRUE(automate.addTransition(3,'a',4));
  EXPECT_TRUE(automate.addTransition(3,'b',5));
  EXPECT_TRUE(automate.addTransition(3,'b',6));
  EXPECT_TRUE(automate.addTransition(4,'a',1));
  EXPECT_TRUE(automate.addTransition(5,'a',6));
  EXPECT_TRUE(automate.addTransition(5,'b',3));
  EXPECT_TRUE(automate.addTransition(6,'a',2));
  EXPECT_TRUE(automate.addTransition(6,'a',7));
  EXPECT_TRUE(automate.addTransition(7,'a',5));
  
  const std::string mot = "b";
  EXPECT_FALSE(automate.match(mot));

}

TEST(AutomatonTestMatch, WordIsNotRecognized){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  automate.setStateFinal(3);
  EXPECT_TRUE(automate.addState(4));
  automate.setStateInitial(4);
  EXPECT_TRUE(automate.addState(5));
  EXPECT_TRUE(automate.addState(6));
  automate.setStateFinal(6);
  EXPECT_TRUE(automate.addState(7));
  automate.setStateInitial(7);
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(0,'a',2));
  EXPECT_TRUE(automate.addTransition(1,'b',3));
  EXPECT_TRUE(automate.addTransition(2,'a',3));
  EXPECT_TRUE(automate.addTransition(3,'a',4));
  EXPECT_TRUE(automate.addTransition(3,'b',5));
  EXPECT_TRUE(automate.addTransition(3,'b',6));
  EXPECT_TRUE(automate.addTransition(4,'a',1));
  EXPECT_TRUE(automate.addTransition(5,'a',6));
  EXPECT_TRUE(automate.addTransition(5,'b',3));
  EXPECT_TRUE(automate.addTransition(6,'a',2));
  EXPECT_TRUE(automate.addTransition(6,'a',7));
  EXPECT_TRUE(automate.addTransition(7,'a',5));
  
  const std::string mot = "aba";
  EXPECT_FALSE(automate.match(mot));

}

TEST(AutomatonTestMatch, WordEmptyNotRecognized){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  automate.setStateFinal(3);
  EXPECT_TRUE(automate.addState(4));
  automate.setStateInitial(4);
  EXPECT_TRUE(automate.addState(5));
  EXPECT_TRUE(automate.addState(6));
  automate.setStateFinal(6);
  EXPECT_TRUE(automate.addState(7));
  automate.setStateInitial(7);
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(0,'a',2));
  EXPECT_TRUE(automate.addTransition(1,'b',3));
  EXPECT_TRUE(automate.addTransition(2,'a',3));
  EXPECT_TRUE(automate.addTransition(3,'a',4));
  EXPECT_TRUE(automate.addTransition(3,'b',5));
  EXPECT_TRUE(automate.addTransition(3,'b',6));
  EXPECT_TRUE(automate.addTransition(4,'a',1));
  EXPECT_TRUE(automate.addTransition(5,'a',6));
  EXPECT_TRUE(automate.addTransition(5,'b',3));
  EXPECT_TRUE(automate.addTransition(6,'a',2));
  EXPECT_TRUE(automate.addTransition(6,'a',7));
  EXPECT_TRUE(automate.addTransition(7,'a',5));
  
  const std::string mot = "";
  EXPECT_FALSE(automate.match(mot));

}

TEST(AutomatonTestMatch, WordEmptyRecognized){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  automate.setStateFinal(3);
  EXPECT_TRUE(automate.addState(4));
  automate.setStateInitial(4);
  EXPECT_TRUE(automate.addState(5));
  EXPECT_TRUE(automate.addState(6));
  automate.setStateFinal(6);
  EXPECT_TRUE(automate.addState(7));
  automate.setStateInitial(7);
  automate.setStateFinal(7);
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(0,'a',2));
  EXPECT_TRUE(automate.addTransition(1,'b',3));
  EXPECT_TRUE(automate.addTransition(2,'a',3));
  EXPECT_TRUE(automate.addTransition(3,'a',4));
  EXPECT_TRUE(automate.addTransition(3,'b',5));
  EXPECT_TRUE(automate.addTransition(3,'b',6));
  EXPECT_TRUE(automate.addTransition(4,'a',1));
  EXPECT_TRUE(automate.addTransition(5,'a',6));
  EXPECT_TRUE(automate.addTransition(5,'b',3));
  EXPECT_TRUE(automate.addTransition(6,'a',2));
  EXPECT_TRUE(automate.addTransition(6,'a',7));
  EXPECT_TRUE(automate.addTransition(7,'a',5));
  
  const std::string mot = "";
  EXPECT_TRUE(automate.match(mot));

}

/**
 * Tests createDeterministic
 */

TEST(AutomatonTestCreateDeterministic, Simple){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addState(3));
  automate.setStateFinal(3);
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(0,'a',2));
  EXPECT_TRUE(automate.addTransition(1,'a',3));

  
   EXPECT_EQ(automate.countStates(),(std::size_t)4);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)3);
  fa::Automaton autoDeter = fa::Automaton::createDeterministic(automate);
  EXPECT_EQ(autoDeter.countStates(),(std::size_t)3);
  EXPECT_EQ(autoDeter.countTransitions(),(std::size_t)2);
  EXPECT_TRUE(autoDeter.isDeterministic());
  const std::string mot = "aa";
  EXPECT_TRUE(automate.match(mot));
  EXPECT_TRUE(autoDeter.match(mot));

}

TEST(AutomatonTestCreateDeterministic, Simple2){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addState(3));
  automate.setStateFinal(3);
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(0,'a',2));
  EXPECT_TRUE(automate.addTransition(1,'b',3));
  EXPECT_TRUE(automate.addTransition(1,'a',2));

  
   EXPECT_EQ(automate.countStates(),(std::size_t)4);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)4);
  fa::Automaton autoDeter = fa::Automaton::createDeterministic(automate);
  EXPECT_EQ(autoDeter.countStates(),(std::size_t)4);
  EXPECT_EQ(autoDeter.countTransitions(),(std::size_t)4);
  EXPECT_TRUE(autoDeter.isDeterministic());
  const std::string mot = "ab";
  EXPECT_TRUE(automate.match(mot));
  EXPECT_TRUE(autoDeter.match(mot));

}

TEST(AutomatonTestCreateDeterministic, Td3Ex9){
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  EXPECT_TRUE(automate.addState(4));
  automate.setStateFinal(4);
  EXPECT_TRUE(automate.addTransition(0,'a',0));
  EXPECT_TRUE(automate.addTransition(0,'b',0));
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(1,'a',2));
  EXPECT_TRUE(automate.addTransition(1,'b',2));
  EXPECT_TRUE(automate.addTransition(2,'a',3));
  EXPECT_TRUE(automate.addTransition(2,'b',3));
  EXPECT_TRUE(automate.addTransition(3,'a',4));
  EXPECT_TRUE(automate.addTransition(3,'b',4));
  
  EXPECT_EQ(automate.countStates(),(std::size_t)5);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)9);
  fa::Automaton autoDeter = fa::Automaton::createDeterministic(automate);
  EXPECT_EQ(autoDeter.countStates(),(std::size_t)16);
  EXPECT_EQ(autoDeter.countTransitions(),(std::size_t)32);
  EXPECT_TRUE(autoDeter.isDeterministic());
  const std::string mot = "aaabab";
  EXPECT_TRUE(automate.match(mot));
  EXPECT_TRUE(autoDeter.match(mot));

}

TEST(AutomatonTestCreateDeterministic, Td2Ex7) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a')); 
  EXPECT_TRUE(automate.addSymbol('b')); 
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  EXPECT_TRUE(automate.addState(4));
  automate.setStateFinal(4);
  EXPECT_TRUE(automate.addTransition(0, 'a', 1));
  EXPECT_TRUE(automate.addTransition(0, 'b', 2));
  EXPECT_TRUE(automate.addTransition(1, 'a', 0));
  EXPECT_TRUE(automate.addTransition(2, 'b', 0));
  EXPECT_TRUE(automate.addTransition(2, 'a', 3));
  EXPECT_TRUE(automate.addTransition(3, 'a', 2));
  EXPECT_TRUE(automate.addTransition(1, 'b', 3));
  EXPECT_TRUE(automate.addTransition(3, 'b', 1));
  EXPECT_TRUE(automate.addTransition(1, 'a', 4));
  EXPECT_TRUE(automate.addTransition(4, 'b', 2));
  EXPECT_EQ(automate.countStates(),(std::size_t)5);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)10);
  fa::Automaton autoDeter = fa::Automaton::createDeterministic(automate);
  EXPECT_EQ(autoDeter.countStates(),(std::size_t)5);
  EXPECT_EQ(autoDeter.countTransitions(),(std::size_t)10);
  EXPECT_TRUE(autoDeter.isDeterministic());
  const std::string mot = "baba";
  EXPECT_TRUE(automate.match(mot));
  EXPECT_TRUE(autoDeter.match(mot));
}

TEST(AutomatonTestCreateDeterministic, Td2Ex8) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0)); 
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  automate.setStateFinal(3);
  EXPECT_TRUE(automate.addTransition(0, 'a', 1));
  EXPECT_TRUE(automate.addTransition(0, 'a', 2));
  EXPECT_TRUE(automate.addTransition(2, 'a', 2));
  EXPECT_TRUE(automate.addTransition(2, 'b', 3));
  EXPECT_TRUE(automate.addTransition(1, 'a', 3));
  EXPECT_TRUE(automate.addTransition(3, 'b', 1));
  EXPECT_EQ(automate.countStates(),(std::size_t)4);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)6);
  fa::Automaton autoDeter = fa::Automaton::createDeterministic(automate);
  EXPECT_EQ(autoDeter.countStates(),(std::size_t)7);
  EXPECT_EQ(autoDeter.countTransitions(),(std::size_t)11);
  EXPECT_TRUE(autoDeter.isDeterministic());
  const std::string mot = "aabbaba";
  EXPECT_TRUE(automate.match(mot));
  EXPECT_TRUE(autoDeter.match(mot));
  const std::string mot2 = "a";
  EXPECT_TRUE(automate.match(mot2));
  EXPECT_TRUE(autoDeter.match(mot2));
}

TEST(AutomatonTestCreateDeterministic, OneStateInitialZeroTransition) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0)); 
  automate.setStateInitial(0);
  EXPECT_EQ(automate.countStates(),(std::size_t)1);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)0);
  fa::Automaton autoDeter = fa::Automaton::createDeterministic(automate);
  EXPECT_EQ(autoDeter.countStates(),(std::size_t)1);
  EXPECT_EQ(autoDeter.countTransitions(),(std::size_t)0);
  EXPECT_TRUE(autoDeter.isDeterministic());
  EXPECT_TRUE(automate.isLanguageEmpty());
  EXPECT_TRUE(autoDeter.isLanguageEmpty());
}

TEST(AutomatonTestCreateDeterministic, NoStateInitial){  
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addState(3));
  automate.setStateFinal(3);
  EXPECT_TRUE(automate.addTransition(0,'a',1));
  EXPECT_TRUE(automate.addTransition(0,'a',2));
  EXPECT_TRUE(automate.addTransition(1,'b',3));
  EXPECT_TRUE(automate.addTransition(1,'a',2));

  
  EXPECT_EQ(automate.countStates(),(std::size_t)4);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)4);
  fa::Automaton autoDeter = fa::Automaton::createDeterministic(automate);
  EXPECT_EQ(autoDeter.countStates(),(std::size_t)1);
  EXPECT_EQ(autoDeter.countTransitions(),(std::size_t)0);
  EXPECT_TRUE(autoDeter.isDeterministic());

}

TEST(AutomatonTestCreateDeterministic, Empty){  
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0));
  EXPECT_TRUE(automate.addTransition(0,'a',0));

  
  EXPECT_EQ(automate.countStates(),(std::size_t)1);
  EXPECT_EQ(automate.countTransitions(),(std::size_t)1);
  fa::Automaton autoDeter = fa::Automaton::createDeterministic(automate);
  EXPECT_EQ(autoDeter.countStates(),(std::size_t)1);
  EXPECT_EQ(autoDeter.countTransitions(),(std::size_t)0);
  EXPECT_TRUE(autoDeter.isDeterministic());

}

/**
 * Tests isIncludedIn
 */

TEST(AutomatonTestIsIncludedIn, ExempleVideoFalse) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0)); 
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1)); 
  automate.setStateFinal(1);
  EXPECT_TRUE(automate.addTransition(0, 'a', 1));
  EXPECT_TRUE(automate.addTransition(1, 'b', 1));

  fa::Automaton automate2;
  EXPECT_TRUE(automate2.addSymbol('a'));
  EXPECT_TRUE(automate2.addSymbol('b'));
  EXPECT_TRUE(automate2.addState(0)); 
  automate2.setStateInitial(0);
  EXPECT_TRUE(automate2.addState(1)); 
  automate2.setStateFinal(1);
  EXPECT_TRUE(automate2.addTransition(0, 'a', 0));
  EXPECT_TRUE(automate2.addTransition(0, 'b', 0));
  EXPECT_TRUE(automate2.addTransition(0, 'a', 1));

  EXPECT_FALSE(automate.isIncludedIn(automate2));

}

TEST(AutomatonTestIsIncludedIn, ExempleVideoTrue) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0)); 
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1)); 
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addTransition(0, 'a', 1));
  EXPECT_TRUE(automate.addTransition(1, 'b', 1));
  EXPECT_TRUE(automate.addTransition(1, 'a', 2));

  fa::Automaton automate2;
  EXPECT_TRUE(automate2.addSymbol('a'));
  EXPECT_TRUE(automate2.addSymbol('b'));
  EXPECT_TRUE(automate2.addState(0)); 
  automate2.setStateInitial(0);
  EXPECT_TRUE(automate2.addState(1)); 
  automate2.setStateFinal(1);
  EXPECT_TRUE(automate2.addTransition(0, 'a', 0));
  EXPECT_TRUE(automate2.addTransition(0, 'b', 0));
  EXPECT_TRUE(automate2.addTransition(0, 'a', 1));

  EXPECT_TRUE(automate.isIncludedIn(automate2));

}

TEST(AutomatonTestIsIncludedIn, L1isEmptyTrue) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0)); 
  automate.setStateInitial(0);

  fa::Automaton automate2;
  EXPECT_TRUE(automate2.addSymbol('a'));
  EXPECT_TRUE(automate2.addSymbol('b'));
  EXPECT_TRUE(automate2.addState(0)); 
  automate2.setStateInitial(0);
  EXPECT_TRUE(automate2.addState(1)); 
  automate2.setStateFinal(1);
  EXPECT_TRUE(automate2.addTransition(0, 'a', 0));
  EXPECT_TRUE(automate2.addTransition(0, 'b', 0));
  EXPECT_TRUE(automate2.addTransition(0, 'a', 1));

  EXPECT_TRUE(automate.isIncludedIn(automate2));

}

TEST(AutomatonTestIsIncludedIn, L1AndL2AreEmptyTrue) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0)); 
  automate.setStateInitial(0);

  fa::Automaton automate2;
  EXPECT_TRUE(automate2.addSymbol('a'));
  EXPECT_TRUE(automate2.addSymbol('b'));
  EXPECT_TRUE(automate2.addState(0)); 
  automate2.setStateInitial(0);

  EXPECT_TRUE(automate.isIncludedIn(automate2));

}

TEST(AutomatonTestIsIncludedIn, L2isEmptyFalse) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0)); 
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1)); 
  EXPECT_TRUE(automate.addState(2));
  automate.setStateFinal(2);
  EXPECT_TRUE(automate.addTransition(0, 'a', 1));
  EXPECT_TRUE(automate.addTransition(1, 'b', 1));
  EXPECT_TRUE(automate.addTransition(1, 'a', 2));

  fa::Automaton automate2;
  EXPECT_TRUE(automate2.addSymbol('a'));
  EXPECT_TRUE(automate2.addSymbol('b'));
  EXPECT_TRUE(automate2.addState(0)); 
  automate2.setStateInitial(0);

  EXPECT_FALSE(automate.isIncludedIn(automate2));

}

TEST(AutomatonTestIsIncludedIn, TestRoyerOuSmolinski) { 
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0)); 
  automate.setStateInitial(0);
  automate.setStateFinal(0);
  EXPECT_TRUE(automate.addTransition(0, 'a', 0));

  fa::Automaton automate2;
  EXPECT_TRUE(automate2.addSymbol('c'));
  EXPECT_TRUE(automate2.addSymbol('d'));
  EXPECT_TRUE(automate2.addState(0));
  automate.setStateInitial(0);
  automate2.setStateFinal(0);
  EXPECT_TRUE(automate2.addTransition(0, 'd', 0));

  EXPECT_FALSE(automate.isIncludedIn(automate2));

}

TEST(AutomatonTestIsIncludedIn, TestSmolinskiDifferentsSymbol) { 
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addState(0)); 
  automate.setStateInitial(0);
  automate.setStateFinal(0);
  EXPECT_TRUE(automate.addState(1)); 
  EXPECT_TRUE(automate.addState(2));
  
  EXPECT_TRUE(automate.addTransition(0, 'a', 1));
  EXPECT_TRUE(automate.addTransition(1, 'b', 1));
  EXPECT_TRUE(automate.addTransition(1, 'a', 2));

  fa::Automaton automate2;
  EXPECT_TRUE(automate2.addSymbol('c'));
  EXPECT_TRUE(automate2.addSymbol('d'));
  EXPECT_TRUE(automate2.addState(0)); 
  automate2.setStateInitial(0);
  automate2.setStateFinal(0);
  EXPECT_TRUE(automate2.addState(1)); 
  EXPECT_TRUE(automate2.addTransition(1, 'd', 1));
  EXPECT_TRUE(automate2.addTransition(0, 'c', 1));

  EXPECT_FALSE(automate.isIncludedIn(automate2));

}

TEST(AutomatonTestIsIncludedIn, TestDifferentsSymbolsWithStateInitialAndFinal1) { 
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addSymbol('c'));
  EXPECT_TRUE(automate.addSymbol('d'));
  EXPECT_TRUE(automate.addState(0)); 
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1)); 
  automate.setStateFinal(1);
  
  EXPECT_TRUE(automate.addTransition(0, 'a', 1));

  fa::Automaton automate2;
  EXPECT_TRUE(automate2.addSymbol('a'));
  EXPECT_TRUE(automate2.addState(0)); 
  automate2.setStateInitial(0);
  EXPECT_TRUE(automate2.addState(1)); 
  automate2.setStateFinal(1);
  EXPECT_TRUE(automate2.addTransition(0, 'a', 1));

  EXPECT_FALSE(automate.isIncludedIn(automate2));

}

TEST(AutomatonTestIsIncludedIn, TestDifferentsSymbolsWithStateInitialAndFinal2) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a'));
  EXPECT_TRUE(automate.addSymbol('b'));
  EXPECT_TRUE(automate.addSymbol('c'));
  EXPECT_TRUE(automate.addSymbol('d'));
  EXPECT_TRUE(automate.addState(0)); 
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));   
  automate.setStateFinal(3);
  
  EXPECT_TRUE(automate.addTransition(0, 'a', 1));
  EXPECT_TRUE(automate.addTransition(1, 'a', 1));
  EXPECT_TRUE(automate.addTransition(1, 'b', 2));
  EXPECT_TRUE(automate.addTransition(2, 'c', 3));
  EXPECT_TRUE(automate.addTransition(2, 'd', 3));

  fa::Automaton automate2;
  EXPECT_TRUE(automate2.addSymbol('a'));
  EXPECT_TRUE(automate2.addState(0)); 
  automate2.setStateInitial(0);
  EXPECT_TRUE(automate2.addState(1)); 
  automate2.setStateFinal(1);
  EXPECT_TRUE(automate2.addTransition(0, 'a', 1));

  EXPECT_FALSE(automate.isIncludedIn(automate2));

}



/**
 * Tests createMinimalMoore
 */
TEST(AutomatonTestCreateMinimalMoore, Td5Ex16EverMinimal) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('0')); 
  EXPECT_TRUE(automate.addSymbol('1')); 
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  EXPECT_TRUE(automate.addState(5));
  automate.setStateFinal(5);
  EXPECT_TRUE(automate.addState(4));
  EXPECT_TRUE(automate.addTransition(0, '1', 2));
  EXPECT_TRUE(automate.addTransition(0, '0', 1));
  EXPECT_TRUE(automate.addTransition(1, '0', 4));
  EXPECT_TRUE(automate.addTransition(1, '1', 5));
  EXPECT_TRUE(automate.addTransition(2, '0', 0));
  EXPECT_TRUE(automate.addTransition(2, '1', 0));
  EXPECT_TRUE(automate.addTransition(3, '0', 5));
  EXPECT_TRUE(automate.addTransition(3, '1', 4));
  EXPECT_TRUE(automate.addTransition(4, '0', 3));
  EXPECT_TRUE(automate.addTransition(4, '1', 5));
  EXPECT_TRUE(automate.addTransition(5, '0', 3));
  EXPECT_TRUE(automate.addTransition(5, '1', 4));
  fa::Automaton autoMinimal = fa::Automaton::createMinimalMoore(automate);
  EXPECT_EQ(autoMinimal.countStates(),(std::size_t)6);
  EXPECT_EQ(autoMinimal.countTransitions(),(std::size_t)12);

  const std::string mot = "11010100";
  EXPECT_TRUE(automate.match(mot));
  EXPECT_TRUE(autoMinimal.match(mot));

}

TEST(AutomatonTestCreateMinimalMoore, Td5Ex17) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a')); 
  EXPECT_TRUE(automate.addSymbol('b')); 
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  EXPECT_TRUE(automate.addState(4));
  automate.setStateFinal(4);
  EXPECT_TRUE(automate.addState(5));
  automate.setStateFinal(5);
  EXPECT_TRUE(automate.addTransition(0, 'a', 1));
  EXPECT_TRUE(automate.addTransition(0, 'b', 3));
  EXPECT_TRUE(automate.addTransition(1, 'a', 0));
  EXPECT_TRUE(automate.addTransition(1, 'b', 2));
  EXPECT_TRUE(automate.addTransition(2, 'b', 5));
  EXPECT_TRUE(automate.addTransition(2, 'a', 1));
  EXPECT_TRUE(automate.addTransition(3, 'a', 1));
  EXPECT_TRUE(automate.addTransition(3, 'b', 4));
  EXPECT_TRUE(automate.addTransition(4, 'a', 0));
  EXPECT_TRUE(automate.addTransition(4, 'b', 4));
  EXPECT_TRUE(automate.addTransition(5, 'a', 1));
  EXPECT_TRUE(automate.addTransition(5, 'b', 4));
  fa::Automaton autoMinimal = fa::Automaton::createMinimalMoore(automate);
  EXPECT_EQ(autoMinimal.countStates(),(std::size_t)3);
  EXPECT_EQ(autoMinimal.countTransitions(),(std::size_t)6);
  const std::string mot = "ababbaabbb";
  EXPECT_TRUE(automate.match(mot));
  EXPECT_TRUE(autoMinimal.match(mot));

}

TEST(AutomatonTestCreateMinimalMoore, Td6Ex21) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a')); 
  EXPECT_TRUE(automate.addSymbol('b')); 
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  EXPECT_TRUE(automate.addState(4));
  EXPECT_TRUE(automate.addState(5));
  automate.setStateFinal(5);
  EXPECT_TRUE(automate.addState(6));
  EXPECT_TRUE(automate.addState(7));
  automate.setStateFinal(7);
  EXPECT_TRUE(automate.addTransition(1, 'a', 2));
  EXPECT_TRUE(automate.addTransition(1, 'b', 4));
  EXPECT_TRUE(automate.addTransition(2, 'a', 5));
  EXPECT_TRUE(automate.addTransition(2, 'b', 4));
  EXPECT_TRUE(automate.addTransition(3, 'a', 7));
  EXPECT_TRUE(automate.addTransition(3, 'b', 4));
  EXPECT_TRUE(automate.addTransition(4, 'a', 6));
  EXPECT_TRUE(automate.addTransition(4, 'b', 5));
  EXPECT_TRUE(automate.addTransition(5, 'b', 3));
  EXPECT_TRUE(automate.addTransition(6, 'a', 3));
  EXPECT_TRUE(automate.addTransition(6, 'b', 4));
  EXPECT_TRUE(automate.addTransition(7, 'b', 3));
  fa::Automaton autoMinimal = fa::Automaton::createMinimalMoore(automate);
  EXPECT_EQ(autoMinimal.countStates(),(std::size_t)5);
  EXPECT_EQ(autoMinimal.countTransitions(),(std::size_t)10);

  const std::string mot = "abbbbabbbaba";
  EXPECT_TRUE(automate.match(mot));
  EXPECT_TRUE(autoMinimal.match(mot));

}

TEST(AutomatonTestCreateMinimalMoore, OneStateInitialAndFinal) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a')); 
  EXPECT_TRUE(automate.addSymbol('b')); 
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  automate.setStateFinal(0);
  EXPECT_TRUE(automate.addTransition(0, 'a', 0));
  EXPECT_TRUE(automate.addTransition(0, 'b', 0));

  fa::Automaton autoMinimal = fa::Automaton::createMinimalMoore(automate);
  EXPECT_EQ(autoMinimal.countStates(),(std::size_t)1);
  EXPECT_EQ(autoMinimal.countTransitions(),(std::size_t)2);

  const std::string mot = "abbbbabbbaba";
  EXPECT_TRUE(automate.match(mot));
  EXPECT_TRUE(autoMinimal.match(mot));

}

TEST(AutomatonTestCreateMinimalMoore, OneStateInitial) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a')); 
  EXPECT_TRUE(automate.addSymbol('b')); 
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addTransition(0, 'a', 0));
  EXPECT_TRUE(automate.addTransition(0, 'b', 0));

  fa::Automaton autoMinimal = fa::Automaton::createMinimalMoore(automate);
  EXPECT_EQ(autoMinimal.countStates(),(std::size_t)1);
  EXPECT_EQ(autoMinimal.countTransitions(),(std::size_t)2);

  const std::string mot = "abbbbabbbaba";
  EXPECT_FALSE(automate.match(mot));
  EXPECT_FALSE(autoMinimal.match(mot));

}

TEST(AutomatonTestCreateMinimalMoore, OneStateFinal) { 
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a')); 
  EXPECT_TRUE(automate.addSymbol('b')); 
  EXPECT_TRUE(automate.addState(0));
  automate.setStateFinal(0);
  EXPECT_TRUE(automate.addTransition(0, 'a', 0));
  EXPECT_TRUE(automate.addTransition(0, 'b', 0));

  fa::Automaton autoMinimal = fa::Automaton::createMinimalMoore(automate);
  EXPECT_EQ(autoMinimal.countStates(),(std::size_t)1);
  EXPECT_EQ(autoMinimal.countTransitions(),(std::size_t)2);

  const std::string mot = "aaaabbb";
  EXPECT_FALSE(automate.match(mot));
  EXPECT_FALSE(autoMinimal.match(""));
  EXPECT_TRUE(autoMinimal.isLanguageEmpty());

}


/**
 * Tests createMinimalBrzozowski
 */
TEST(AutomatonTestCreateMinimalBrzozowski, Td5Ex16EverMinimal) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('0')); 
  EXPECT_TRUE(automate.addSymbol('1')); 
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  EXPECT_TRUE(automate.addState(5));
  automate.setStateFinal(5);
  EXPECT_TRUE(automate.addState(4));
  EXPECT_TRUE(automate.addTransition(0, '1', 2));
  EXPECT_TRUE(automate.addTransition(0, '0', 1));
  EXPECT_TRUE(automate.addTransition(1, '0', 4));
  EXPECT_TRUE(automate.addTransition(1, '1', 5));
  EXPECT_TRUE(automate.addTransition(2, '0', 0));
  EXPECT_TRUE(automate.addTransition(2, '1', 0));
  EXPECT_TRUE(automate.addTransition(3, '0', 5));
  EXPECT_TRUE(automate.addTransition(3, '1', 4));
  EXPECT_TRUE(automate.addTransition(4, '0', 3));
  EXPECT_TRUE(automate.addTransition(4, '1', 5));
  EXPECT_TRUE(automate.addTransition(5, '0', 3));
  EXPECT_TRUE(automate.addTransition(5, '1', 4));
  fa::Automaton autoMinimal = fa::Automaton::createMinimalBrzozowski(automate);
  EXPECT_EQ(autoMinimal.countStates(),(std::size_t)6);
  EXPECT_EQ(autoMinimal.countTransitions(),(std::size_t)12);

  const std::string mot = "11010100";
  EXPECT_TRUE(automate.match(mot));
  EXPECT_TRUE(autoMinimal.match(mot));

}

TEST(AutomatonTestCreateMinimalBrzozowski, Td5Ex17) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a')); 
  EXPECT_TRUE(automate.addSymbol('b')); 
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addState(1));
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  EXPECT_TRUE(automate.addState(4));
  automate.setStateFinal(4);
  EXPECT_TRUE(automate.addState(5));
  automate.setStateFinal(5);
  EXPECT_TRUE(automate.addTransition(0, 'a', 1));
  EXPECT_TRUE(automate.addTransition(0, 'b', 3));
  EXPECT_TRUE(automate.addTransition(1, 'a', 0));
  EXPECT_TRUE(automate.addTransition(1, 'b', 2));
  EXPECT_TRUE(automate.addTransition(2, 'b', 5));
  EXPECT_TRUE(automate.addTransition(2, 'a', 1));
  EXPECT_TRUE(automate.addTransition(3, 'a', 1));
  EXPECT_TRUE(automate.addTransition(3, 'b', 4));
  EXPECT_TRUE(automate.addTransition(4, 'a', 0));
  EXPECT_TRUE(automate.addTransition(4, 'b', 4));
  EXPECT_TRUE(automate.addTransition(5, 'a', 1));
  EXPECT_TRUE(automate.addTransition(5, 'b', 4));
  fa::Automaton autoMinimal = fa::Automaton::createMinimalBrzozowski(automate);
  EXPECT_EQ(autoMinimal.countStates(),(std::size_t)3);
  EXPECT_EQ(autoMinimal.countTransitions(),(std::size_t)6);
  const std::string mot = "ababbaabbb";
  EXPECT_TRUE(automate.match(mot));
  EXPECT_TRUE(autoMinimal.match(mot));

}

TEST(AutomatonTestCreateMinimalBrzozowski, Td6Ex21) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a')); 
  EXPECT_TRUE(automate.addSymbol('b')); 
  EXPECT_TRUE(automate.addState(1));
  automate.setStateInitial(1);
  EXPECT_TRUE(automate.addState(2));
  EXPECT_TRUE(automate.addState(3));
  EXPECT_TRUE(automate.addState(4));
  EXPECT_TRUE(automate.addState(5));
  automate.setStateFinal(5);
  EXPECT_TRUE(automate.addState(6));
  EXPECT_TRUE(automate.addState(7));
  automate.setStateFinal(7);
  EXPECT_TRUE(automate.addTransition(1, 'a', 2));
  EXPECT_TRUE(automate.addTransition(1, 'b', 4));
  EXPECT_TRUE(automate.addTransition(2, 'a', 5));
  EXPECT_TRUE(automate.addTransition(2, 'b', 4));
  EXPECT_TRUE(automate.addTransition(3, 'a', 7));
  EXPECT_TRUE(automate.addTransition(3, 'b', 4));
  EXPECT_TRUE(automate.addTransition(4, 'a', 6));
  EXPECT_TRUE(automate.addTransition(4, 'b', 5));
  EXPECT_TRUE(automate.addTransition(5, 'b', 3));
  EXPECT_TRUE(automate.addTransition(6, 'a', 3));
  EXPECT_TRUE(automate.addTransition(6, 'b', 4));
  EXPECT_TRUE(automate.addTransition(7, 'b', 3));
  fa::Automaton autoMinimal = fa::Automaton::createMinimalBrzozowski(automate);
  EXPECT_EQ(autoMinimal.countStates(),(std::size_t)5);
  EXPECT_EQ(autoMinimal.countTransitions(),(std::size_t)10);

  const std::string mot = "abbbbabbbaba";
  EXPECT_TRUE(automate.match(mot));
  EXPECT_TRUE(autoMinimal.match(mot));
  std::ostream& os = std::cout;
  autoMinimal.prettyPrint(os);

}

TEST(AutomatonTestCreateMinimalBrzozowski, OneStateInitialAndFinal) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a')); 
  EXPECT_TRUE(automate.addSymbol('b')); 
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  automate.setStateFinal(0);
  EXPECT_TRUE(automate.addTransition(0, 'a', 0));
  EXPECT_TRUE(automate.addTransition(0, 'b', 0));

  fa::Automaton autoMinimal = fa::Automaton::createMinimalBrzozowski(automate);
  EXPECT_EQ(autoMinimal.countStates(),(std::size_t)1);
  EXPECT_EQ(autoMinimal.countTransitions(),(std::size_t)2);

  const std::string mot = "abbbbabbbaba";
  EXPECT_TRUE(automate.match(mot));
  EXPECT_TRUE(autoMinimal.match(mot));
  

}

TEST(AutomatonTestCreateMinimalBrzozowski, OneStateInitial) {
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a')); 
  EXPECT_TRUE(automate.addSymbol('b')); 
  EXPECT_TRUE(automate.addState(0));
  automate.setStateInitial(0);
  EXPECT_TRUE(automate.addTransition(0, 'a', 0));
  EXPECT_TRUE(automate.addTransition(0, 'b', 0));

  fa::Automaton autoMinimal = fa::Automaton::createMinimalBrzozowski(automate);
  EXPECT_EQ(autoMinimal.countStates(),(std::size_t)2);
  EXPECT_EQ(autoMinimal.countTransitions(),(std::size_t)4);

  const std::string mot = "abbbbabbbaba";
  EXPECT_FALSE(automate.match(mot));
  EXPECT_FALSE(autoMinimal.match(mot));
  EXPECT_FALSE(autoMinimal.match(""));
  std::ostream& os = std::cout;
  autoMinimal.prettyPrint(os);

}

TEST(AutomatonTestCreateMinimalBrzozowski, OneStateFinal) { 
  fa::Automaton automate;
  EXPECT_TRUE(automate.addSymbol('a')); 
  EXPECT_TRUE(automate.addSymbol('b')); 
  EXPECT_TRUE(automate.addState(0));
  automate.setStateFinal(0);
  EXPECT_TRUE(automate.addTransition(0, 'a', 0));
  EXPECT_TRUE(automate.addTransition(0, 'b', 0));

  fa::Automaton autoMinimal = fa::Automaton::createMinimalBrzozowski(automate);
  EXPECT_EQ(autoMinimal.countStates(),(std::size_t)2);
  EXPECT_EQ(autoMinimal.countTransitions(),(std::size_t)4);

  const std::string mot = "aaaabbb";
  EXPECT_FALSE(automate.match(mot));
  EXPECT_FALSE(autoMinimal.match(""));
  EXPECT_TRUE(autoMinimal.isLanguageEmpty());
  std::ostream& os = std::cout;
  autoMinimal.prettyPrint(os);

}
int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}