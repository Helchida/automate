#include "Automaton.h"
#include <string>
#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator> 
#include <map>

//Morgan LECLERCQ
namespace fa {

  Automaton::Automaton() {
  }

  /*Fonction qui vérifie la validité d'un automate
    Alphabet ne doit pas être vide
    Etats ne doivent pas être vide
    return true si l'automate est valide, false sinon
  */
  bool Automaton::isValid() const {
    return !etats.empty() && !alphabet.empty();
  }

  /*Fonction qui ajoute un symbole à l'alphabet
    return true si le symbole est ajouté, false sinon
  */
  bool Automaton::addSymbol(char symbol){
    if(isgraph(symbol)==0 || symbol==fa::Epsilon){
      return false;
    }
    for(int i=0; i<abs(alphabet.size());i++){
      if(symbol==alphabet[i]){
        return false;
      }
    }
    alphabet.push_back(symbol);
    return true;
  }

  /*Fonction qui supprime un symbole à l'alphabet
    return true si le symbole est supprimé false sinon
  */
  bool Automaton::removeSymbol(char symbol){
    if(isgraph(symbol)==0 || symbol==fa::Epsilon){
      return false;
    }
    if(hasSymbol(symbol)){
      for(int i=0; i<abs(alphabet.size());i++){
        if(symbol==alphabet[i]){
          alphabet.erase(alphabet.begin() + i);
        }
      }
      std::vector<int> trans_a_suppr;
      for(int j=0;j<abs(transitions.size());j++){
        if(transitions[j].symbole==symbol){
          trans_a_suppr.push_back(j);
        }
      }
      for(int k=abs(trans_a_suppr.size())-1;k>=0;k--){
        removeTransition(transitions[trans_a_suppr[k]].etat_depart,transitions[trans_a_suppr[k]].symbole,transitions[trans_a_suppr[k]].etat_arrive);
      }
      return true;
    }
    return false;
  }

  /*Fonction qui verifie la présence d'un symbole dans l'alphabet
    return true si le symbole est présent, false sinon
  */
  bool Automaton::hasSymbol(char symbol) const{
    if(isgraph(symbol)==0 || symbol==fa::Epsilon){
      return false;
    }
    for(int i=0; i<abs(alphabet.size());i++){
      if(symbol==alphabet[i]){
        return true;
      }
    }
    return false;
  }

  /*Fonction qui compte le nombre de symbole dans l'alphabet
    return le nombre de symbole
  */
  std::size_t Automaton::countSymbols() const{
      return abs(alphabet.size());
  }

  /*Fonction qui ajoute un état
    return true si l'état est ajouté, false sinon
  */
  bool Automaton::addState(int state){
    if(state<0){
      return false;
    }
    for(int i=0;i<abs(etats.size());i++){
      if(etats[i]==state){
        return false;
      }
    }
    etats.push_back(state);
    return true;
  }
  /*Fonction qui supprime un état
    return true si l'état est supprimé, false sinon
  */
  bool Automaton::removeState(int state){
    if(hasState(state)){
      for(int i=0;i<abs(etats.size());i++){
        if(etats[i]==state){
          etats.erase(etats.begin() + i);
        }
      }
      for(int j=0; j<abs(etats_initiaux.size());j++){
        if(etats_initiaux[j]==state){
          etats_initiaux.erase(etats_initiaux.begin() + j);
        }
      }
      for(int k=0; k<abs(etats_finaux.size());k++){
        if(etats_finaux[k]==state){
          etats_finaux.erase(etats_finaux.begin() + k);
        }
      }
      std::vector<int> trans_a_suppr;
      for(int l=0;l<abs(transitions.size());l++){
        if((transitions[l].etat_depart==state)||(transitions[l].etat_arrive==state)){
          trans_a_suppr.push_back(l);
        }
      }
      for(int m=abs(trans_a_suppr.size())-1;m>=0;m--){
        removeTransition(transitions[trans_a_suppr[m]].etat_depart,transitions[trans_a_suppr[m]].symbole,transitions[trans_a_suppr[m]].etat_arrive);
      }
      return true;
    }
    return false;
  }

  /*Fonction qui vérifie la présence d'un état
    return true si l'état est présent, false sinon
  */
  bool Automaton::hasState(int state) const{
    for(int i=0;i<abs(etats.size());i++){
      if(state==etats[i]){
        return true;
      }
    }
    return false;
  }

  /*Fonction qui compte le nombre d'état
    return le nombre d'état
  */
  std::size_t Automaton::countStates() const{
    return abs(etats.size());
  }

  /*Fonction qui rend un état non-initial en initial*/
  void Automaton::setStateInitial(int state){
    if(!hasState(state)){
      return;
    }
    for(int i=0;i<abs(etats_initiaux.size());i++){
      if(etats_initiaux[i]==state){
        return;
      }
    }
    etats_initiaux.push_back(state);
    return;
  }

  /*Fonction qui dit si un état est initial
    return true si état est initial, false sinon
  */
  bool Automaton::isStateInitial(int state) const{
    if(!hasState(state)){
      return false;
    }
    for(int i=0;i<abs(etats_initiaux.size());i++){
      if(etats_initiaux[i]==state){
        return true;
      }
    }
    return false;
  }

  /*Fonction qui rend un état non-final en final*/
  void Automaton::setStateFinal(int state){
    if(!hasState(state)){
      return;
    }
    for(int i=0;i<abs(etats_finaux.size());i++){
      if(etats_finaux[i]==state){
        return;
      }
    }
    etats_finaux.push_back(state);
    return;
  }

  /*Fonction qui dit si un état est final
    return true si état est final, false sinon
  */
  bool Automaton::isStateFinal(int state) const{
    if(!hasState(state)){
      return false;
    }
    for(int i=0;i<abs(etats_finaux.size());i++){
      if(etats_finaux[i]==state){
        return true;
      }
    }
    return false;
  }

  /*Fonction qui ajoute une transiton
    return true si la transition est ajouté, false sinon
  */
  bool Automaton::addTransition(int from, char alpha, int to){
    if(alpha==fa::Epsilon){
      if(!hasState(from) || !hasState(to)){
        return false;
      }
    }else{
      if(!hasState(from) || (!hasSymbol(alpha)) || !hasState(to)){
        return false;
      }
    }
    if(hasTransition(from,alpha,to)){
      return false;
    }
    struct transition trans_add;
    trans_add.etat_depart=from;
    trans_add.etat_arrive=to;
    trans_add.symbole=alpha;
    transitions.push_back(trans_add);
    return true;
  }

  /*Fonction qui supprime une transiton
    return true si la transition est supprimé, false sinon
  */
  bool Automaton::removeTransition(int from, char alpha, int to){
    if(!hasTransition(from,alpha,to)){
      return false;
    }
    for(int i=0;i<abs(transitions.size());i++){
      if(transitions[i].etat_depart==from && transitions[i].etat_arrive==to && transitions[i].symbole==alpha){
        transitions.erase(transitions.begin() + i);
        return true;
      }
    }
    return false;
  }

  /*Fonction qui vérifie la présence d'une transition
    return true si la transition est présente, false sinon
  */
  bool Automaton::hasTransition(int from, char alpha, int to) const{
    for(int i=0; i<abs(transitions.size());i++){
      if(transitions[i].etat_depart==from && transitions[i].etat_arrive==to && transitions[i].symbole==alpha){
        return true;
      }
    }
    return false;
  }

  /*Fonction qui compte le nombre de transition
    return le nombre de transition de l'automate
  */
  std::size_t Automaton::countTransitions() const{
    return abs(transitions.size());
  }

  /*Fonction qui affiche dans la console l'automate*/
  void Automaton::prettyPrint(std::ostream& os) const{
    os << "Initial states:\n";
    os << "\t\t";
    for(int i=0;i<abs(etats_initiaux.size());i++){
      os << etats_initiaux[i] << " ";
    }
    os << "\nFinal states:\n";
    os << "\t\t";
    for(int i=0;i<abs(etats_finaux.size());i++){
      os << etats_finaux[i] << " ";
    }
    os << "\nTransitions:\n";
    for(int j=0;j<abs(etats.size());j++){
      os << "\tFor state " << etats[j] << ":\n";
      for(int k=0;k<abs(alphabet.size());k++){
        os << "\t\tFor letter " << alphabet[k] << ": ";
        for(int l=0;l<abs(transitions.size());l++){
          if(transitions[l].etat_depart==etats[j] && transitions[l].symbole==alphabet[k]){
            os << transitions[l].etat_arrive << " ";
          }
        }
        os << "\n";
      }
      os << "\n";
    }
  }

 
  /*Fonction qui affiche un graph de l'automate sous format dot*/
  void Automaton::dotPrint(std::ostream& os) const{
    std::string str="digraph test {rankdir=LR;";
    for(int j=0;j<abs(etats_initiaux.size());j++){
      str+="node [shape=point]; o"+std::to_string(j)+";";
    }
    str+="node [shape=doublecircle];";
    for(int i=0;i<abs(etats_finaux.size());i++){
      str+=" "+std::to_string(etats_finaux[i]);
    }
    str+=";";
    str+="node [shape = circle];";
      for(int j=0;j<abs(etats_initiaux.size());j++){
        str+="o"+std::to_string(j)+" -> "+std::to_string(etats_initiaux[j]);
        str+=";";
      }
        for(int l=0;l<abs(transitions.size());l++){
          str+=std::to_string(transitions[l].etat_depart)+" -> "+std::to_string(transitions[l].etat_arrive)+" [label = "+transitions[l].symbole+"];";
        }
        str+="}";
    if(!freopen("automate.dot","w",stdout)){
      std::cout << "freopen failed";
      exit(1);
    };
    os << str.c_str() << std::endl;
    fclose(stdout);
    if(!system("dot -Tps automate.dot -o automate.ps")){
      std::cout << "system failed";
      exit(2);
    }
    if(!system("xdg-open automate.ps")){
      std::cout << "system failed";
      exit(3);
    }
  }

  
  /*Fonction qui vérifie la présence d'une epsilon transition
    return true si une epsilon transition est présente, false sinon
  */
  bool Automaton::hasEpsilonTransition() const{
    assert(isValid());
    for(int i=0; i<abs(transitions.size());i++){
      if(transitions[i].symbole==fa::Epsilon){
        return true;
      }
    }
    return false;
  }

  /*Fonction qui verifie si un automate est deterministe
    return true si l'automate est deterministe, false sinon
  */
  bool Automaton::isDeterministic() const{
    if(!isValid()){
      return false;
    }
    if(etats_initiaux.size()!=1){
      return false;
    }
    for(int i=0;i<abs(transitions.size());i++){
      for(int j=0;j<abs(transitions.size());j++){
        if(transitions[j].etat_depart==transitions[i].etat_depart && transitions[j].symbole==transitions[i].symbole && transitions[j].etat_arrive!=transitions[i].etat_arrive){
          return false;
        }
      }
    }
    return true;
  }

  /*Fonction qui vérifie si un automate est complet
    retun true si l'automte est complet
  */
  bool Automaton::isComplete() const{
    if(!isValid()){
      return false;
    }
    for(int i=0;i<abs(alphabet.size());i++){
      for(int j=0;j<abs(etats.size());j++){
        bool is_good=false;
        for(int k=0;k<abs(transitions.size());k++){
          if(transitions[k].etat_depart==etats[j] && transitions[k].symbole==alphabet[i]){
            is_good=true;
          }
        }
        if(is_good==false){
          return false;
        }
      }
    }
    return true;
  }

  /*Fonction qui compléte un automate
  return un automate complet
  */
  Automaton Automaton::createComplete(const Automaton& automaton){
    assert(automaton.isValid());
    if(automaton.isComplete()){
      return automaton;
    }
    fa::Automaton autoComplete;
    for(int i=0;i<abs(automaton.alphabet.size());i++){
      autoComplete.addSymbol(automaton.alphabet[i]);
    }
    for(int j=0;j<abs(automaton.etats.size());j++){
      autoComplete.addState(automaton.etats[j]);
    }
    for(int k=0;k<abs(automaton.etats_initiaux.size());k++){
      autoComplete.setStateInitial(automaton.etats_initiaux[k]);
    }
    for(int l=0;l<abs(automaton.etats_finaux.size());l++){
      autoComplete.setStateFinal(automaton.etats_finaux[l]);
    }
    for(int m=0;m<abs(automaton.transitions.size());m++){
      autoComplete.addTransition(automaton.transitions[m].etat_depart,automaton.transitions[m].symbole,automaton.transitions[m].etat_arrive);
    }
    int indice_poubelle=0;
    bool indice_is_good=false;
    while(indice_is_good==false){
      if(automaton.hasState(indice_poubelle)){
        indice_poubelle++;
      }else{
        indice_is_good=true;
      }
    }
    autoComplete.addState(indice_poubelle);
    for(int i=0;i<abs(autoComplete.etats.size());i++){
      for(int j=0;j<abs(autoComplete.alphabet.size());j++){
        bool is_good=false;
        for(int k=0;k<abs(autoComplete.transitions.size());k++){
          if(autoComplete.transitions[k].etat_depart==autoComplete.etats[i] && autoComplete.transitions[k].symbole==autoComplete.alphabet[j]){
            is_good=true;
          }
        }
        if(is_good==false){
          autoComplete.addTransition(autoComplete.etats[i],autoComplete.alphabet[j],indice_poubelle);
        }
      }
    }
    return autoComplete;
  }


  /*Fonction qui créer l'automate complémentaire d'un automate donné
    return l'automate complémentaire
  */
  Automaton Automaton::createComplement(const Automaton& automaton){
    assert(automaton.isValid());
    fa::Automaton autoComplement=automaton;
    if(!automaton.isDeterministic()){
      autoComplement=fa::Automaton::createDeterministic(automaton);
    }
    if(!autoComplement.isComplete()){
      autoComplement=fa::Automaton::createComplete(autoComplement);
    }
    std::vector<int> new_etats_finaux;
    for(int i=0;i<abs(autoComplement.etats.size());i++){
      if(std::find(autoComplement.etats_finaux.begin(),autoComplement.etats_finaux.end(),autoComplement.etats[i]) == autoComplement.etats_finaux.end()){
        new_etats_finaux.push_back(autoComplement.etats[i]);
      }
    }
    autoComplement.etats_finaux.erase(autoComplement.etats_finaux.begin(),autoComplement.etats_finaux.end());
    for(int j=0;j<abs(new_etats_finaux.size());j++){
      autoComplement.etats_finaux.push_back(new_etats_finaux[j]);
    }
    return autoComplement;
  }
  
  /*Fonction qui créer l'automate mirroir d'un automate donné
    return l'automate mirroir
  */
  Automaton Automaton::createMirror(const Automaton& automaton){
    assert(automaton.isValid());
    fa::Automaton autoMirror=automaton;
    std::vector<int> new_etats_finaux;
    for(int i=0;i<abs(autoMirror.etats_initiaux.size());i++){
      new_etats_finaux.push_back(autoMirror.etats_initiaux[i]);      
    }
    std::vector<int> new_etats_initiaux;
    for(int i=0;i<abs(autoMirror.etats_finaux.size());i++){
      new_etats_initiaux.push_back(autoMirror.etats_finaux[i]);      
    }
    std::vector<struct transition> new_transitions;
    for(int i=0;i<abs(autoMirror.transitions.size());i++){
      struct transition trans_add;
      trans_add.etat_depart=autoMirror.transitions[i].etat_arrive;
      trans_add.etat_arrive=autoMirror.transitions[i].etat_depart;
      trans_add.symbole=autoMirror.transitions[i].symbole;
      new_transitions.push_back(trans_add);      
    }
    autoMirror.etats_finaux.erase(autoMirror.etats_finaux.begin(),autoMirror.etats_finaux.end());
    for(int j=0;j<abs(new_etats_finaux.size());j++){
      autoMirror.etats_finaux.push_back(new_etats_finaux[j]);
    }
    autoMirror.etats_initiaux.erase(autoMirror.etats_initiaux.begin(),autoMirror.etats_initiaux.end());
    for(int j=0;j<abs(new_etats_initiaux.size());j++){
      autoMirror.etats_initiaux.push_back(new_etats_initiaux[j]);
    }
    autoMirror.transitions.erase(autoMirror.transitions.begin(),autoMirror.transitions.end());
    for(int j=0;j<abs(new_transitions.size());j++){
      autoMirror.transitions.push_back(new_transitions[j]);
    }
    return autoMirror;
  }

  /*Fonction récursive pour isLanguageEmpty
  */
  bool Automaton::isVisited(std::vector<int> final_state, std::vector<struct transition> all_trans, std::vector<struct transition> trans_visited, transition t){
    trans_visited.push_back(t);
    if(std::find(final_state.begin(),final_state.end(),t.etat_depart) != final_state.end()){
      return false;
    }
    if(std::find(final_state.begin(),final_state.end(),t.etat_arrive) != final_state.end()){
      return false;
    }
    bool is_visited=true;
    for(int i=0;i<abs(all_trans.size());i++){
      if(all_trans[i].etat_depart==t.etat_arrive){
        bool have_already_trans=false;
        for(int j=0;j<abs(trans_visited.size());j++){
          if(trans_visited[j].etat_depart==all_trans[i].etat_depart && trans_visited[j].symbole==all_trans[i].symbole && trans_visited[j].etat_arrive==all_trans[i].etat_arrive){
            have_already_trans=true;
          }
        }
        if(!have_already_trans){
          is_visited=isVisited(final_state,all_trans,trans_visited,all_trans[i]);
          if(!is_visited){
            return is_visited;
          }
        }
      }
    }
    return is_visited;
  }

  /*Fonction qui vérifie si un langage est vide
    return true si le langage est vide, false sinon
  */
  bool Automaton::isLanguageEmpty() const{
    if(etats_finaux.size()==0 || etats_initiaux.size()==0){
      return true;
    }
    std::vector<struct transition> trans_visited;
    bool is_empty=true;
    for(int i=0; i<abs(etats_initiaux.size());i++){
      for(int k=0; k<abs(etats_finaux.size());k++){
        if(etats_initiaux[i]==etats_finaux[k]){
          return false;
        }
      }
      for(int j=0;j<abs(transitions.size());j++){ 
        if(etats_initiaux[i]==transitions[j].etat_depart){
          is_empty=is_empty&&isVisited(etats_finaux,transitions,trans_visited,transitions[j]);
        }
      }
    }
    return is_empty;
  }

  
  /*Fonction récursive pour removeNonAccessibleStates
  */
  std::vector<int> Automaton::isVisitedNonAccessiblesStates(std::vector<struct transition> transis,std::vector<int> tabStateVisited,int etat_actuel) const{
    std::vector<int> tabEtatVoisins;
    for(size_t i=0;i<transis.size();i++){
      if(etat_actuel==transis[i].etat_depart){
        tabEtatVoisins.push_back(transis[i].etat_arrive);
      }
    }
    if(tabEtatVoisins.size()!=0){
      for(size_t j=0;j<tabEtatVoisins.size();j++){
        bool is_present=false;
        for(size_t k=0;k<tabStateVisited.size();k++){
          if(tabStateVisited[k]==tabEtatVoisins[j]){
            is_present=true;
          }
        }
        if(!is_present){
          tabStateVisited.push_back(tabEtatVoisins[j]);
          tabStateVisited=isVisitedNonAccessiblesStates(transis,tabStateVisited,tabEtatVoisins[j]);
        }
      }
    }
    return tabStateVisited;

  }

  /*Fonction qui supprime les états non accessibles d'un automate
  */
  void Automaton::removeNonAccessibleStates(){
    if(etats_initiaux.empty()){
      transitions.clear();
      etats.clear();
      etats_finaux.clear();
      addState(0);
    }else{
      std::vector<int> tabStateVisited;
      for(size_t i=0;i<abs(etats_initiaux.size());i++){
        tabStateVisited.push_back(etats_initiaux[i]);
        tabStateVisited=isVisitedNonAccessiblesStates(transitions,tabStateVisited,etats_initiaux[i]);
      }
      for(size_t j=0;j<abs(etats.size());j++){
        bool is_present=false;
        for(size_t k=0;k<tabStateVisited.size();k++){
          if(etats[j]==tabStateVisited[k]){
            is_present=true;
            break;
          }
        }
        if(!is_present){
          removeState(etats[j]);
          j=0;
        }
      }
      if(!isValid()){
        if(abs(etats.size())==0){
          addState(0);
        }
      }
    }
  }

  /*Fonction récursive pour removeNonCoAccessibleStates
  */
  std::vector<int> Automaton::isVisitedNonCoAccessiblesStates(std::vector<struct transition> transis,std::vector<int> tabStateVisited,int etat_actuel) const{
    std::vector<int> tabEtatVoisins;
    for(size_t i=0;i<transis.size();i++){
      if(etat_actuel==transis[i].etat_arrive){
        tabEtatVoisins.push_back(transis[i].etat_depart);
      }
    }
    if(tabEtatVoisins.size()!=0){
      for(size_t j=0;j<tabEtatVoisins.size();j++){
        bool is_present=false;
        for(size_t k=0;k<tabStateVisited.size();k++){
          if(tabStateVisited[k]==tabEtatVoisins[j]){
            is_present=true;
          }
        }
        if(!is_present){
          tabStateVisited.push_back(tabEtatVoisins[j]);
          tabStateVisited=isVisitedNonCoAccessiblesStates(transis,tabStateVisited,tabEtatVoisins[j]);
        }
      }
    }
    return tabStateVisited;
  }

  /*Fonction qui supprime les états non co-accessibles d'un automate
  */
  void Automaton::removeNonCoAccessibleStates(){
    if(etats_finaux.empty()){
      transitions.clear();
      etats.clear();
      etats_initiaux.clear();
      addState(0);
    }else{
      std::vector<int> tabStateVisited;
      for(size_t i=0;i<abs(etats_finaux.size());i++){
        tabStateVisited.push_back(etats_finaux[i]);
        tabStateVisited=isVisitedNonCoAccessiblesStates(transitions,tabStateVisited,etats_finaux[i]);
      }
      for(size_t j=0;j<abs(etats.size());j++){
        bool is_present=false;
        for(size_t k=0;k<tabStateVisited.size();k++){
          if(etats[j]==tabStateVisited[k]){
            is_present=true;
            break;
          }
        }
        if(!is_present){
          removeState(etats[j]);
          j=0;
        }
      }
      if(!isValid()){
        if(abs(etats.size())==0){
          addState(0);
        }
      }
    }
  }

  /*Fonction qui créer le produit synchronisé de deux automates
    return le produit synchronisé d'automates
  */
  Automaton Automaton::createProduct(const Automaton& lhs, const Automaton& rhs){
    assert(lhs.isValid());
    assert(rhs.isValid());
    fa::Automaton autoProduct;
    for(int i=0;i<abs(lhs.countSymbols());i++){
      if(std::find(rhs.alphabet.begin(),rhs.alphabet.end(),lhs.alphabet[i]) != rhs.alphabet.end()){
        autoProduct.addSymbol(lhs.alphabet[i]);
        std::cout << lhs.alphabet[i] << " ADD ! \n";
      }
    }
    if(lhs.countSymbols()!=0 && rhs.countSymbols()!=0){
      int nombre_etats = lhs.countStates()*rhs.countStates();
      for(int j=0;j<abs(nombre_etats);j++){
        autoProduct.addState(j);
      }
      int indice=0;
      std::map<int,std::vector<int>> map_etats;
      for(int i=0;i<abs(lhs.countStates());i++){
        for(int j=0;j<abs(rhs.countStates());j++){
          std::vector<int> pair_etat;
          pair_etat.push_back(lhs.etats[i]);
          pair_etat.push_back(rhs.etats[j]);
          map_etats.insert(std::pair<int,std::vector<int>>(indice,pair_etat));
          if(lhs.isStateInitial(lhs.etats[i])){
            if(rhs.isStateInitial(rhs.etats[j])){
              autoProduct.setStateInitial(indice);
            }
          }
          if(lhs.isStateFinal(lhs.etats[i])){
            if(rhs.isStateFinal(rhs.etats[j])){
              autoProduct.setStateFinal(indice);
            }
          }
          if(!(lhs.countSymbols()==rhs.countSymbols() && lhs.countSymbols()==autoProduct.countSymbols() && rhs.countSymbols()==autoProduct.countSymbols())){
            if((lhs.isStateInitial(lhs.etats[i]) && lhs.isStateFinal(lhs.etats[i])) || (rhs.isStateInitial(rhs.etats[j]) && rhs.isStateFinal(rhs.etats[j]))){   
              autoProduct.setStateInitial(indice);
              autoProduct.setStateFinal(indice);
            }
          }
          indice++;
        }
      }
      for(std::map<int,std::vector<int>>::iterator i =map_etats.begin();i!=map_etats.end();i++){
        for(int j=0;j<abs(autoProduct.countSymbols());j++){
          std::vector<int> transitions_lhs;
          std::vector<int> transitions_rhs;
          for(int k=0;k<abs(lhs.countTransitions());k++){
            if(lhs.transitions[k].etat_depart==i->second[0] && lhs.transitions[k].symbole==autoProduct.alphabet[j]){
              transitions_lhs.push_back(lhs.transitions[k].etat_arrive);
            }
          }
          for(int k=0;k<abs(rhs.countTransitions());k++){
            if(rhs.transitions[k].etat_depart==i->second[1] && rhs.transitions[k].symbole==autoProduct.alphabet[j]){
              transitions_rhs.push_back(rhs.transitions[k].etat_arrive);
            } 
          }
          for(int l=0;l<abs(transitions_lhs.size());l++){
            for(int m=0;m<abs(transitions_rhs.size());m++){
              for(std::map<int,std::vector<int>>::iterator it_arrive =map_etats.begin();it_arrive!=map_etats.end();it_arrive++){
                if(transitions_lhs[l]==it_arrive->second[0] && transitions_rhs[m]==it_arrive->second[1]){
                  autoProduct.addTransition(i->first,autoProduct.alphabet[j],it_arrive->first);
                }
              }
            }
          }
        }
      }
    }else{
      autoProduct.addSymbol('a');
      autoProduct.addState(0);
      autoProduct.setStateInitial(0);
      return autoProduct;
    }
    if(!autoProduct.isValid()){
      autoProduct.addSymbol('a');
      autoProduct.addState(0);
      autoProduct.setStateInitial(0);
      return autoProduct;
    }
    return autoProduct;
  }

  /*Fonction qui vérifie si l'intersection de deux automates est vide
    return true si intersection vide sinon false
  */
  bool Automaton::hasEmptyIntersectionWith(const Automaton& other) const{
    fa::Automaton autoProduct=createProduct(*this,other);
    if(autoProduct.isLanguageEmpty()){
      return true;
    }
    return false;
  }

  /*Fonction récursive pour readString
  */
  bool Automaton::rec_readString(int etat,std::set<int> &path,const std::string &mot,int indice_avant,fa::Automaton automate){
    if(indice_avant+1==abs(mot.size())){
      if(path.count(etat)==0){
        path.insert(etat);
      }
      return true;
    }else{
      for(int j=0;j<abs(automate.countTransitions());j++){
        if(automate.transitions[j].etat_depart==etat && automate.transitions[j].symbole==mot.at(indice_avant+1)){
          rec_readString(automate.transitions[j].etat_arrive,path,mot,indice_avant+1,automate);
        }
      }
    }
    return false;
  }

  /*Fonction qui renvoie la liste des états d'arrivée pour la lecture d'un mot
    return un set de int*/ 
  std::set<int> Automaton::readString(const std::string &word) const{
    std::set<int> chemin;
    if(word.length()==0){
      for(int i=0;i<abs(etats_initiaux.size());i++){
        chemin.insert(etats_initiaux[i]);
      }
      return chemin;
    }
    for(int i=0;i<abs(etats_initiaux.size());i++){
      for(int j=0;j<abs(countTransitions());j++){
        if(transitions[j].etat_depart==etats_initiaux[i] && transitions[j].symbole==word.at(0)){
          rec_readString(transitions[j].etat_arrive,chemin,word,0,*this);
        }
      }
    }
    return chemin;
  }

  /*Fonction qui reconnait ou non un mot
    return true si le mot est reconnu, sinon false
  */
  bool Automaton::match(const std::string& word) const{
    std::set<int> etats_arrivees=readString(word);
    for(auto it2=etats_arrivees.begin(); it2!=etats_arrivees.end();++it2){
      if(std::find(etats_finaux.begin(),etats_finaux.end(),*it2) != etats_finaux.end()){
        return true;
      }
    }
    return false;
  }

  /*Getter de la liste des états
    return un vector de int
  */
   std::vector<int> Automaton::get_etats() const{
    return etats;
  }

  /*Getter de la liste des transitions
    return un vector de transition
  */
  std::vector<struct transition> Automaton::get_transitions() const{
    return transitions;
  }

  /*Getter de la liste des symboles
    return un vector de char
  */
  std::vector<char> Automaton::get_alphabet() const{
    return alphabet;
  }

  /*Getter de la liste des états finaux
    return un vector de int
  */
  std::vector<int> Automaton::get_etats_finaux() const{
    return etats_finaux;
  }

  /*Getter de la liste des états initiaux
    return un vector de int
  */
  std::vector<int> Automaton::get_etats_initiaux() const{
    return etats_initiaux;
  }

  /*Fonction qui vérifie la possibilité d'ajouter un état pour createDeterministic
  */
  bool can_add_state_deterministic(std::map<int,std::vector<std::vector<int>>> map_etats, std::vector<int> tab_etats){
    if(tab_etats.size()==0){
      return false;
    }
    for(std::map<int,std::vector<std::vector<int>>>::iterator i =map_etats.begin();i!=map_etats.end();i++){
      if(i->second[0].size()==tab_etats.size()){
        int cpt=0;
        for(int j=0;j<abs(i->second[0].size());j++){
          for(int k=0;k<abs(tab_etats.size());k++){
            if(i->second[0][j]==tab_etats[k]){
              cpt++;
            }
          }     
        }
        if(cpt==abs(i->second[0].size())){
          return false;
        }
      }
    }
    return true;
  }

  /*Fonction qui créer un état pour createDeterministic
  */
  std::map<int,std::vector<std::vector<int>>> createStateOfDeterministic(std::map<int,std::vector<std::vector<int>>> map_etats,int indice,fa::Automaton automate,bool isStatePresent){
    std::map<int,std::vector<std::vector<int>>> other_map=map_etats;
    if(isStatePresent){
      return map_etats;
    }else{
      for(std::map<int,std::vector<std::vector<int>>>::iterator i =map_etats.begin();i!=map_etats.end();i++){
        if(i->first==indice){
          int taille_map=abs(map_etats.size());
          std::vector<int> tab_etats;
          std::vector<std::vector<int>> tab_transi_tab_etats;
          for(int k=0;k<abs(automate.get_alphabet().size());k++){
            i->second.push_back(tab_etats);
            tab_transi_tab_etats.push_back(tab_etats);
          }
          for(int j=0;j<abs(i->second[0].size());j++){
            for(int k=0;k<abs(automate.get_alphabet().size());k++){
              for(int l=0;l<abs(automate.get_transitions().size());l++){
                if(automate.get_transitions()[l].symbole==automate.get_alphabet()[k] && automate.get_transitions()[l].etat_depart==i->second[0][j]){
                  if(std::find(i->second[k+1].begin(),i->second[k+1].end(),automate.get_transitions()[l].etat_arrive) == i->second[k+1].end()){
                    i->second[k+1].push_back(automate.get_transitions()[l].etat_arrive);
                    tab_etats.push_back(automate.get_transitions()[l].etat_arrive);
                  }
                }
              }
              for(int m=0;m<abs(tab_etats.size());m++){
                tab_transi_tab_etats[k].push_back(tab_etats[m]);
              }
              tab_etats.clear();
            }
          }
          int cpt=0;
          for(int n=0;n<abs(tab_transi_tab_etats.size());n++){
            if(can_add_state_deterministic(map_etats,tab_transi_tab_etats[n])){
              std::vector<std::vector<int>> tab_of_tab_etats;
              tab_of_tab_etats.push_back(tab_transi_tab_etats[n]);
              map_etats.insert(std::pair<int,std::vector<std::vector<int>>>(taille_map+cpt,tab_of_tab_etats));
              isStatePresent=false;
              cpt++;
            }
          }  
          other_map=createStateOfDeterministic(map_etats,indice+1,automate,isStatePresent); 
        }
      }
    }
    return other_map;
  }

  /*Fonction qui rend un automate en automate deterministe
    return un automate deterministe
  */
  Automaton Automaton::createDeterministic(const Automaton& other){
    assert(other.isValid());
    fa::Automaton automate;
    for(int i=0;i<abs(other.alphabet.size());i++){
      automate.alphabet.push_back(other.alphabet[i]);
    }
    for(int i=0;i<abs(other.etats.size());i++){
      automate.etats.push_back(other.etats[i]);
    }
    for(int i=0;i<abs(other.transitions.size());i++){
      automate.transitions.push_back(other.transitions[i]);
    }
    for(int i=0;i<abs(other.etats_initiaux.size());i++){
      automate.etats_initiaux.push_back(other.etats_initiaux[i]);
    }
    if(abs(other.etats_initiaux.size())==0){
      fa::Automaton autoReturn;
      autoReturn.addState(0);
      autoReturn.setStateInitial(0);
      for(int i=0;i<abs(other.alphabet.size());i++){
        autoReturn.addSymbol(other.alphabet[i]);
      } 
      return autoReturn;
    }
    for(int i=0;i<abs(other.etats_finaux.size());i++){
      automate.etats_finaux.push_back(other.etats_finaux[i]);
    }
    if(automate.isDeterministic()){
      return automate;
    }else{
      int indice=0;
      std::map<int,std::vector<std::vector<int>>> map_etats;
      std::vector<std::vector<int>> tab_initiaux;
      std::vector<int> elements_init;
      for(int i=0;i<abs(automate.get_etats_initiaux().size());i++){
       elements_init.push_back(automate.get_etats_initiaux()[i]);
      }
      tab_initiaux.push_back(elements_init);
      map_etats.insert(std::pair<int,std::vector<std::vector<int>>>(indice,tab_initiaux));
      map_etats=fa::createStateOfDeterministic(map_etats,indice,automate,false);
      for(auto it=map_etats.begin(); it!=map_etats.end();++it){
        std::cout  << it->first << " : ";
        for(int i=0;i<abs(it->second.size());i++){
          std::cout << "[";
          for(int j=0;j<abs(it->second[i].size());j++){
            std::cout << it->second[i][j] << " ";
          }
          std::cout << "]";
        }
        std::cout << "\n";
      }
      fa::Automaton autoDeter;
      for(int i=0;i<abs(automate.countSymbols());i++){
        autoDeter.addSymbol(automate.get_alphabet()[i]);
      }
      for(auto it=map_etats.begin(); it!=map_etats.end();++it){
        autoDeter.addState(it->first);
      }
      for(auto it=map_etats.begin(); it!=map_etats.end();++it){
        if(it->first==0){
          autoDeter.setStateInitial(0);
        }
        for(int j=0;j<abs(it->second[0].size());j++){
          if(std::find(automate.etats_finaux.begin(),automate.etats_finaux.end(),it->second[0][j]) != automate.etats_finaux.end()){
            autoDeter.setStateFinal(it->first);
          }
        }
        for(int k=0;k<abs(it->second.size())-1;k++){
          for(auto it2=map_etats.begin(); it2!=map_etats.end();++it2){
            if(abs(it->second[k+1].size())==abs(it2->second[0].size())){
              bool is_state=true;
              for(int l=0;l<abs(it->second[k+1].size());l++){
                if(std::find(it2->second[0].begin(),it2->second[0].end(),it->second[k+1][l]) == it2->second[0].end()){
                  is_state=false;
                }
              }
              if(is_state){
                autoDeter.addTransition(it->first,autoDeter.get_alphabet()[k],it2->first);
              }
            }
          }
        }
      }
      return autoDeter;
    }
  }

  /*Fonction qui vérifie si le langage d'un automate est reconnu par un autre automate
  return true si il est aussi reconnu sinon false
  */
  bool Automaton::isIncludedIn(const Automaton& other) const{
    assert(isValid() && other.isValid());
    fa::Automaton otherComplement=fa::Automaton::createComplement(other);
    fa::Automaton autoProduct=fa::Automaton::createProduct(*this,otherComplement);
    std::ostream& os = std::cout;
    autoProduct.prettyPrint(os);
    if(autoProduct.isLanguageEmpty()){
      return true;
    }
    return false;
  }

  /*Fonction qui minimise un automate via l'algorithme de Moore
  return un automate minimisé
  */
  Automaton Automaton::createMinimalMoore(const Automaton& other){
    assert(other.isValid());
    fa::Automaton autoMini=other;
    if(!other.isDeterministic()){
      autoMini=fa::Automaton::createDeterministic(other);
      if(!autoMini.isDeterministic()){
        return autoMini;
      }
    }
    if(!autoMini.isComplete()){
      autoMini=fa::Automaton::createComplete(autoMini);
    }
    std::map<int,std::vector<std::vector<int>>> tabMinimize;
    for(int i=0;i<abs(autoMini.etats.size());i++){
      std::vector<int> tab_trans;
      if(std::find(autoMini.etats_finaux.begin(), autoMini.etats_finaux.end(), autoMini.etats[i]) != autoMini.etats_finaux.end()){
        tab_trans.push_back(2);
      }else{
        tab_trans.push_back(1);
      }
      std::vector<std::vector<int>> tab_of_tab_classes;
      tab_of_tab_classes.push_back(tab_trans);
      tabMinimize.insert(std::pair<int,std::vector<std::vector<int>>>(autoMini.etats[i],tab_of_tab_classes));
    }
    bool is_minimize=false;
    int indice_etape=0;
    while(!is_minimize){
      for(int i=0;i<abs(autoMini.etats.size());i++){
        for(int k=0;k<abs(autoMini.alphabet.size());k++){
          for(int l=0;l<abs(autoMini.transitions.size());l++){
            if(autoMini.transitions[l].etat_depart==autoMini.etats[i] && autoMini.transitions[l].symbole==autoMini.alphabet[k]){
              for(auto it=tabMinimize.begin();it!=tabMinimize.end();++it){
                if(it->first==autoMini.transitions[l].etat_arrive){
                  for(auto it2=tabMinimize.begin();it2!=tabMinimize.end();++it2){
                    if(it2->first==autoMini.etats[i]){
                      it2->second[indice_etape].push_back(it->second[indice_etape][0]);
                    }
                  }
                }
              }
            }
          }
        }
      }
      for(auto it3=tabMinimize.begin(); it3!=tabMinimize.end();++it3){
        if(it3->first==autoMini.etats[0]){
          std::vector<int> tab_trans_zero;
          tab_trans_zero.push_back(1);
          it3->second.push_back(tab_trans_zero);
        }else{
          std::vector<int> tab_trans;
          tab_trans.push_back(0);
          it3->second.push_back(tab_trans);
        }
      }
      int num_classe=1;
      for(auto it3=tabMinimize.begin(); it3!=tabMinimize.end();++it3){
        if(it3->first!=autoMini.etats[0]){
          bool found_class=false;
          for(auto it4=tabMinimize.begin(); it4!=tabMinimize.end();++it4){
            if(it3->first!=it4->first){
              bool are_same=true;
              for(int i=0;i<abs(autoMini.alphabet.size()+1);i++){
                if(it3->second[indice_etape][i]!=it4->second[indice_etape][i]){
                  are_same=false;
                }
              }
              if(are_same){
                if(it4->second[indice_etape+1][0]!=0){
                  if(it3->second[indice_etape+1][0]==0){
                    it3->second[indice_etape+1][0]=it4->second[indice_etape+1][0];
                    found_class=true;
                  }
                }
              }
            }
          }
          if(!found_class){
            num_classe++;
            it3->second[indice_etape+1][0]=num_classe;
          }
        }
      }
      bool is_terminated=true;
      for(auto it3=tabMinimize.begin(); it3!=tabMinimize.end();++it3){
        if(it3->second[indice_etape][0]!=it3->second[indice_etape+1][0]){
          is_terminated=false;
        }
      }
      if(is_terminated){
        is_minimize=true;
      }
      indice_etape++;
    }
    fa::Automaton autoMiniFinish;
    for(int i=0;i<abs(autoMini.alphabet.size());i++){
      autoMiniFinish.addSymbol(autoMini.alphabet[i]);
    }
    for(auto it=tabMinimize.begin(); it!=tabMinimize.end();++it){
      autoMiniFinish.addState(it->second[indice_etape][0]);
      if(std::find(autoMini.etats_initiaux.begin(),autoMini.etats_initiaux.end(),it->first) != autoMini.etats_initiaux.end()){
        autoMiniFinish.setStateInitial(it->second[indice_etape][0]);
      }
      if(std::find(autoMini.etats_finaux.begin(),autoMini.etats_finaux.end(),it->first) != autoMini.etats_finaux.end()){
        autoMiniFinish.setStateFinal(it->second[indice_etape][0]);
      }
    }
    for(auto it=tabMinimize.begin(); it!=tabMinimize.end();++it){
      for(int j=0;j<abs(autoMini.alphabet.size());j++){
        autoMiniFinish.addTransition(it->second[indice_etape][0],autoMini.alphabet[j],it->second[indice_etape-1][j+1]);
      }
    }
    for(auto it=tabMinimize.begin(); it!=tabMinimize.end();++it){
      std::cout  << it->first << " : ";
      for(int i=0;i<abs(it->second.size());i++){
        std::cout << "[";
        for(int j=0;j<abs(it->second[i].size());j++){
          std::cout << it->second[i][j] << " ";
        }
        std::cout << "]";
      }
      std::cout << "\n";
    }
    return autoMiniFinish;
  }

  /*Fonction qui minimise un automate via l'algorithme de Brzozowski
  return un automate minimisé
  */
  Automaton Automaton::createMinimalBrzozowski(const Automaton& other){
    assert(other.isValid());
    fa::Automaton automateBegin=other;
    fa::Automaton automateFirstMirror=createMirror(automateBegin);
    fa::Automaton automateFirstDeter;
    if(automateFirstMirror.isDeterministic()){
      automateFirstDeter=automateFirstMirror;
    }else{
      automateFirstDeter=createDeterministic(automateFirstMirror);
    }
    automateFirstDeter.removeNonAccessibleStates();
    fa::Automaton autoComplete;
    if(automateFirstDeter.isComplete()){
      autoComplete=automateFirstDeter;
    }else{
      autoComplete=createComplete(automateFirstDeter);
    }
    fa::Automaton automateSecondMirror=createMirror(autoComplete);
    fa::Automaton automateMinimize;
    if(automateSecondMirror.isDeterministic()){
      automateMinimize=automateSecondMirror;
    }else{
      automateMinimize=createDeterministic(automateSecondMirror);
    }
    fa::Automaton automateSecondComplete;
    automateSecondComplete=fa::Automaton::createComplete(automateMinimize);
    return automateSecondComplete;
  }
}